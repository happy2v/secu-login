'use strict';

angular
.module('seculogin')
.controller('SignupCtrl',
['$scope', '$state', 'accountSvc', function ($scope, $state, accountSvc) {
	$scope.param = {};

	$scope.sendSignup = function() {
		accountSvc.signup().save($scope.param)
		.$promise.then(
			function (response) {
				console.log(response);
				$scope.message = "Success: " + response.data;
				$state.go('app.signin');
			},
			function (response) {
				console.log(response);
				$scope.message = "Error: " + response.data + response.status + " " + response.statusText;
			}
		);
	}
}
])
.controller('SigninCtrl',
['$scope', '$cookies', '$state', 'accountSvc', function ($scope, $cookies, $state, accountSvc) {
	$scope.param = {};

	$scope.sendSignin = function() {
		accountSvc.signin().save($scope.param)
		.$promise.then(
			function (response) {
				console.log(response.data);
				$scope.message = "Success: " + response.data;
				var score = response.score;
				if (score == 1) {
					$state.go('app.signincaptcha');
				} else if (score >= 2) {
					$state.go('app.signinemail');
				} else {
					$cookies.put('sessionKey', response.data);
					$state.go('app.dashboard');
				}
			},
			function (response) {
				console.log(response);
				$scope.message = "Error: " + response.data + response.status + " " + response.statusText;
			}
		);
	}
}
])
.controller('SigninCaptchaCtrl',
['$scope', '$cookies', '$state', 'accountSvc', function ($scope, $cookies, $state, accountSvc) {
	$scope.param = {};

	$scope.sendSignin = function() {
		accountSvc.signin().save($scope.param)
		.$promise.then(
			function (response) {
				console.log(response.data);
				$scope.message = "Success: " + response.data;
				var score = response.score;
				if (score == 1) {
					$state.go('app.signincaptcha');
				} else if (score >= 4) {
					$state.go('app.signinemail');
				} else {
					$cookies.put('sessionKey', response.data);
					$state.go('app.dashboard');
				}
			},
			function (response) {
				console.log(response);
				$scope.message = "Error: " + response.data + response.status + " " + response.statusText;
			}
		);
	}
}
])
.controller('SigninEmailCtrl',
['$scope', '$cookies', '$state', 'accountSvc', function ($scope, $cookies, $state, accountSvc) {
	$scope.param = {};

	$scope.sendSignin = function() {
		accountSvc.signin().save($scope.param)
		.$promise.then(
			function (response) {
				console.log(response.data);
				$scope.message = "Success: " + response.data;
				var score = response.score;
				if (score == 1) {
					$state.go('app.signincaptcha');
				} else if (score >= 4) {
					$state.go('app.signinemail');
				} else {
					$cookies.put('sessionKey', response.data);
					$state.go('app.dashboard');
				}
			},
			function (response) {
				console.log(response);
				$scope.message = "Error: " + response.data + response.status + " " + response.statusText;
			}
		);
	}
}
])
.controller('SignoutCtrl',
['$scope', '$cookies', '$state', 'accountSvc', function ($scope, $cookies, $state, accountSvc) {
	$scope.param = {};

	$scope.sendSignout = function() {
		var sessionKey = $cookies.get("sessionKey");
		if (sessionKey === undefined) {
			$scope.message = "Not found sessionKey";
			return;
		}
		$scope.param.sessionKey = sessionKey;
		accountSvc.signout().save($scope.param)
		.$promise.then(
			function (response) {
				console.log(response);
				$scope.message = "Success: " + response.data;
				$cookies.remove('sessionKey');
				$state.go('app.signin');
			},
			function (response) {
				console.log(response);
				$scope.message = "Error: " + response.data + response.status + " " + response.statusText;
			}
		);
	}
}
]);
