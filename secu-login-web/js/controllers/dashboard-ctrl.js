'use strict';

angular
.module('seculogin')
.controller('DashboardCtrl2',
['$scope', '$state', '$cookies', '$interval', 'dashboardSvc', function ($scope, $state, $cookies, $interval, dashboardSvc) {
	var sessionKey = $cookies.get("sessionKey");
	if (sessionKey === undefined) {
		$state.go("app.signin");
		return;
	}
	$scope.showBanCount = false;

	$scope.loadBanCount = function() {
		dashboardSvc.banCount().get()
		.$promise.then(
			function (response) {
				var datas = response["chartDatas"];
				if (datas != undefined) {
					$scope.showBanCount = true;

					$scope.plotObj = $.plot($("#flot-line-chart-ban-count"), [datas], {
						series: {
							shadowSize: 0
						},
						yaxis: {
							min: 0,
							max: 5
						},
						xaxis: {
							show: false
						}
					});

					$interval($scope.updateBanCount, 5000);
				}
			},
			function (response) {
				console.log(response);
				$scope.message = "Error: " + response.data + response.status + " " + response.statusText;
			}
		);
	};

	$scope.updateBanCount = function() {
		dashboardSvc.banCount().get()
		.$promise.then(
			function (response) {
				var datas = response["chartDatas"];
				if (datas != undefined) {
					$scope.showBanCount = true;

					$scope.plotObj = $.plot($("#flot-line-chart-ban-count"), [datas], {
						series: {
							shadowSize: 0
						},
						yaxis: {
							min: 0,
							max: 5
						},
						xaxis: {
							show: false
						}
					});
				}
			},
			function (response) {
				console.log(response);
				$scope.message = "Error: " + response.data + response.status + " " + response.statusText;
			}
		);
	}

	$scope.loadBanCount();

}])
.controller('DashboardCtrl',
['$scope', '$state', '$cookies', 'dashboardSvc', function ($scope, $state, $cookies, dashboardSvc) {
	var sessionKey = $cookies.get("sessionKey");
	if (sessionKey === undefined) {
		$state.go("app.signin");
		return;
	}
	$scope.showAccept = false;
	$scope.showAcceptEncoding = false;
	$scope.showAcceptLanguage = false;
	$scope.showCacheControl = false;
	$scope.showConnection = false;
	$scope.showContentLength = false;
	$scope.showContentType = false;
	$scope.showHost = false;
	$scope.showOrigin = false;
	$scope.showPortNumber = false;
	$scope.showPragma = false;
	$scope.showReferer = false;
	$scope.showUserIp = false;
	$scope.showUserAgent = false;

	$scope.showHeaderTable = false;
	$scope.showBanReason = false;
	$scope.showScore = false;

	$scope.loadHeaderRatio = function () {
		dashboardSvc.headerRatio().get()
		.$promise.then(
			function (response) {
				console.log(response);
				$scope.headerRatios = response.headerRatios;
				$scope.showHeaderTable = true;
				$scope.loadBanReason();
			},
			function (response) {
				console.log(response);
				$scope.message = "Error: " + response.data + response.status + " " + response.statusText;
			}
		);
	};

	$scope.loadBanReason = function() {
		dashboardSvc.banReason().get()
		.$promise.then(
			function (response) {
				console.log(response);
				var reasons = response["reasons"];
				if (reasons != undefined) {
					$scope.showBanReason = true;
					var plotObj = $.plot($("#flot-pie-chart-ban-reason"), reasons, {
						series: {
							pie: {
								show: true
							}
						},
						grid: {
							hoverable: true
						},
						tooltip: true,
						tooltipOpts: {
							content: "%p.0%, %s", // show percentages, rounding to 2 decimal places
							shifts: {
								x: 20,
								y: 0
							},
							defaultTheme: false
						}
					});
				}

				$scope.loadScore();
			},
			function (response) {
				console.log(response);
				$scope.message = "Error: " + response.data + response.status + " " + response.statusText;
			}
		);
	};

	$scope.loadScore = function() {
		$scope.param = {};
		$scope.param.sort = [];
		$scope.param.sort.push("id,desc");
		dashboardSvc.score().get($scope.param)
		.$promise.then(
			function (response) {
				console.log(response);
				$scope.showScore = true;
				$scope.scores = response.content;
				$scope.loadChart();
			},
			function (response) {
				console.log(response);
				$scope.message = "Error: " + response.data + response.status + " " + response.statusText;
			}
		);
	};

	$scope.loadChart = function() {
		dashboardSvc.allchart().get()
		.$promise.then(
			function (response) {

				if (response != undefined) {
					console.log(response);
					var acceptData = response["accept"];
					if (acceptData != undefined) {
						$scope.showAccept = true;
						var plotObj = $.plot($("#flot-pie-chart-accept"), acceptData, {
							series: {
								pie: {
									show: true
								}
							},
							grid: {
								hoverable: true
							},
							tooltip: true,
							tooltipOpts: {
								content: "%p.0%, %s", // show percentages, rounding to 2 decimal places
								shifts: {
									x: 20,
									y: 0
								},
								defaultTheme: false
							}
						});
					}
					var acceptEncodingData = response["accept-encoding"];
					if (acceptEncodingData != undefined) {
						$scope.showAcceptEncoding = true;
						var plotObj = $.plot($("#flot-pie-chart-accept-encoding"), acceptEncodingData, {
							series: {
								pie: {
									show: true
								}
							},
							grid: {
								hoverable: true
							},
							tooltip: true,
							tooltipOpts: {
								content: "%p.0%, %s", // show percentages, rounding to 2 decimal places
								shifts: {
									x: 20,
									y: 0
								},
								defaultTheme: false
							}
						});
					}
					var acceptLanguageData = response["accept-language"];
					if (acceptLanguageData != undefined) {
						$scope.showAcceptLanguage = true;
						var plotObj = $.plot($("#flot-pie-chart-accept-language"), acceptLanguageData, {
							series: {
								pie: {
									show: true
								}
							},
							grid: {
								hoverable: true
							},
							tooltip: true,
							tooltipOpts: {
								content: "%p.0%, %s", // show percentages, rounding to 2 decimal places
								shifts: {
									x: 20,
									y: 0
								},
								defaultTheme: false
							}
						});
					}
					var cacheControlData = response["cache-control"];
					if (cacheControlData != undefined) {
						$scope.showCacheControl = true;
						var plotObj = $.plot($("#flot-pie-chart-cache-control"), cacheControlData, {
							series: {
								pie: {
									show: true
								}
							},
							grid: {
								hoverable: true
							},
							tooltip: true,
							tooltipOpts: {
								content: "%p.0%, %s", // show percentages, rounding to 2 decimal places
								shifts: {
									x: 20,
									y: 0
								},
								defaultTheme: false
							}
						});
					}
					var connectionData = response["connection"];
					if (connectionData != undefined) {
						$scope.showConnection = true;
						var plotObj = $.plot($("#flot-pie-chart-connection"), connectionData, {
							series: {
								pie: {
									show: true
								}
							},
							grid: {
								hoverable: true
							},
							tooltip: true,
							tooltipOpts: {
								content: "%p.0%, %s", // show percentages, rounding to 2 decimal places
								shifts: {
									x: 20,
									y: 0
								},
								defaultTheme: false
							}
						});
					}
					var contentLengthData = response["content-length"];
					if (contentLengthData != undefined) {
						$scope.showContentLength = true;
						var plotObj = $.plot($("#flot-pie-chart-content-length"), contentLengthData, {
							series: {
								pie: {
									show: true
								}
							},
							grid: {
								hoverable: true
							},
							tooltip: true,
							tooltipOpts: {
								content: "%p.0%, %s", // show percentages, rounding to 2 decimal places
								shifts: {
									x: 20,
									y: 0
								},
								defaultTheme: false
							}
						});
					}
					var contentTypeData = response["content-type"];
					if (contentTypeData != undefined) {
						$scope.showContentType = true;
						var plotObj = $.plot($("#flot-pie-chart-content-type"), contentTypeData, {
							series: {
								pie: {
									show: true
								}
							},
							grid: {
								hoverable: true
							},
							tooltip: true,
							tooltipOpts: {
								content: "%p.0%, %s", // show percentages, rounding to 2 decimal places
								shifts: {
									x: 20,
									y: 0
								},
								defaultTheme: false
							}
						});
					}
					var hostData = response["host"];
					if (hostData != undefined) {
						$scope.showHost = true;
						var plotObj = $.plot($("#flot-pie-chart-host"), hostData, {
							series: {
								pie: {
									show: true
								}
							},
							grid: {
								hoverable: true
							},
							tooltip: true,
							tooltipOpts: {
								content: "%p.0%, %s", // show percentages, rounding to 2 decimal places
								shifts: {
									x: 20,
									y: 0
								},
								defaultTheme: false
							}
						});
					}
					var originData = response["origin"];
					if (originData != undefined) {
						$scope.showOrigin = true;
						var plotObj = $.plot($("#flot-pie-chart-origin"), originData, {
							series: {
								pie: {
									show: true
								}
							},
							grid: {
								hoverable: true
							},
							tooltip: true,
							tooltipOpts: {
								content: "%p.0%, %s", // show percentages, rounding to 2 decimal places
								shifts: {
									x: 20,
									y: 0
								},
								defaultTheme: false
							}
						});
					}
					var portNumberData = response["port number"];
					if (portNumberData != undefined) {
						$scope.showPortNumber = true;
						var plotObj = $.plot($("#flot-pie-chart-port-number"), portNumberData, {
							series: {
								pie: {
									show: true
								}
							},
							grid: {
								hoverable: true
							},
							tooltip: true,
							tooltipOpts: {
								content: "%p.0%, %s", // show percentages, rounding to 2 decimal places
								shifts: {
									x: 20,
									y: 0
								},
								defaultTheme: false
							}
						});
					}
					var pragmaData = response["pragma"];
					if (pragmaData != undefined) {
						$scope.showPragma = true;
						var plotObj = $.plot($("#flot-pie-chart-pragma"), pragmaData, {
							series: {
								pie: {
									show: true
								}
							},
							grid: {
								hoverable: true
							},
							tooltip: true,
							tooltipOpts: {
								content: "%p.0%, %s", // show percentages, rounding to 2 decimal places
								shifts: {
									x: 20,
									y: 0
								},
								defaultTheme: false
							}
						});
					}
					var refererData = response["referer"];
					if (refererData != undefined) {
						$scope.showReferer = true;
						var plotObj = $.plot($("#flot-pie-chart-referer"), refererData, {
							series: {
								pie: {
									show: true
								}
							},
							grid: {
								hoverable: true
							},
							tooltip: true,
							tooltipOpts: {
								content: "%p.0%, %s", // show percentages, rounding to 2 decimal places
								shifts: {
									x: 20,
									y: 0
								},
								defaultTheme: false
							}
						});
					}
					var userIpData = response["user ip"];
					if (userIpData != undefined) {
						$scope.showUserIp = true;
						var plotObj = $.plot($("#flot-pie-chart-user-ip"), userIpData, {
							series: {
								pie: {
									show: true
								}
							},
							grid: {
								hoverable: true
							},
							tooltip: true,
							tooltipOpts: {
								content: "%p.0%, %s", // show percentages, rounding to 2 decimal places
								shifts: {
									x: 20,
									y: 0
								},
								defaultTheme: false
							}
						});
					}
					var userAgentData = response["user-agent"];
					if (userAgentData != undefined) {
						$scope.showUserAgent = true;
						var plotObj = $.plot($("#flot-pie-chart-user-agent"), userAgentData, {
							series: {
								pie: {
									show: true
								}
							},
							grid: {
								hoverable: true
							},
							tooltip: true,
							tooltipOpts: {
								content: "%p.0%, %s", // show percentages, rounding to 2 decimal places
								shifts: {
									x: 20,
									y: 0
								},
								defaultTheme: false
							}
						});
					}
				}
			},

			function (response) {
				console.log(response);
				$scope.message = "Error: " + response.data + response.status + " " + response.statusText;
			}
		);
	}
	$scope.loadHeaderRatio();
}
]);
