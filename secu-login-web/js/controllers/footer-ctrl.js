'use strict';

angular
.module('seculogin')
.controller('FooterCtrl',
['$scope', '$cookies', function ($scope, $cookies) {
  var sessionKey = $cookies.get("sessionKey");
  if (sessionKey === undefined) {
    $scope.message = "Not found sessionKey";
    return;
  } else {
    $scope.message = sessionKey;
  }
}
]);
