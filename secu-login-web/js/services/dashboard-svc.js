'use strict';

angular
.module('seculogin')
.service('dashboardSvc', ['$resource', 'apiURL', function($resource, apiURL) {
	this.allchart = function() {
		return $resource(apiURL + "pie/chart/all", null, {'update': {method:'PUT'}});
	};
	this.headerRatio = function() {
		return $resource(apiURL + "header/ratio", null, {'update': {method:'PUT'}});
	};
	this.banReason = function(){
		return $resource(apiURL + "reason/ban", null, {'update': {method:'PUT'}});
	};
	this.banCount = function(){
		return $resource(apiURL + "count/ban", null, {'update': {method:'PUT'}});
	};
	this.score = function(){
		return $resource(apiURL + "score", null, {'update': {method:'PUT'}});
	};
}]);
