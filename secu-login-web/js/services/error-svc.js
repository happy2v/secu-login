'use strict';

angular
.module('seculogin')
.service('errorSvc', ['$resource', 'apiURL', function($resource, baseURL) {
	this.serverResource = function() {
		return $resource(apiURL + "error", null, {'update': {method:'PUT'}});
	};
}]);
