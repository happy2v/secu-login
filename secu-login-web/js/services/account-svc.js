'use strict';

angular
.module('seculogin')
.service('accountSvc', ['$resource', 'apiURL', function($resource, apiURL) {
	this.signin = function() {
		return $resource(apiURL + "signin", null, {'update': {method:'PUT'}});
	};
	this.signup = function() {
		return $resource(apiURL + "signup", null, {'update': {method:'PUT'}});
	};
	this.signout = function() {
		return $resource(apiURL + "signout", null, {'update': {method:'PUT'}});
	};
}]);
