'use strict';

angular
.module('seculogin', ['ui.router', 'ngResource', 'ngCookies'])
.config(function($stateProvider, $urlRouterProvider) {
	$stateProvider
	.state('app', {
		url:'/',
		views: {
			'header': {
				templateUrl	: 'views/header.html'
			},
			'content': {
				templateUrl	: 'views/signin.html',
				controller 	: 'SigninCtrl'
			},
			'footer': {
				templateUrl	: 'views/footer.html',
				controller 	: 'FooterCtrl'
			}
		}
	})
	.state('app.dashboard', {
		url:'dashboard',
		views: {
			'content@': {
				templateUrl	: 'views/dashboard.html',
				controller 	: 'DashboardCtrl'
			}
		}
	})
	.state('app.dashboard2', {
		url:'dashboard2',
		views: {
			'content@': {
				templateUrl	: 'views/dashboard2.html',
				controller 	: 'DashboardCtrl2'
			}
		}
	})
	.state('app.signin', {
		url:'signin',
		views: {
			'content@': {
				templateUrl	: 'views/signin.html',
				controller 	: 'SigninCtrl'
			}
		}
	})
	.state('app.signincaptcha', {
		url:'signincaptcha',
		views: {
			'content@': {
				templateUrl	: 'views/signin-captcha.html',
				controller 	: 'SigninCaptchaCtrl'
			}
		}
	})
	.state('app.signinemail', {
		url:'signinemail',
		views: {
			'content@': {
				templateUrl	: 'views/signin-email.html',
				controller 	: 'SigninEmailCtrl'
			}
		}
	})
	.state('app.signinsms', {
		url:'signinsms',
		views: {
			'content@': {
				templateUrl	: 'views/signin-sms.html',
				controller 	: 'SigninSmsCtrl'
			}
		}
	})
	.state('app.signup', {
		url:'signup',
		views: {
			'content@': {
				templateUrl	: 'views/signup.html',
				controller 	: 'SignupCtrl'
			}
		}
	})
	.state('app.error', {
		url:'error',
		views: {
			'content@': {
				templateUrl	: 'views/error.html',
				controller 	: 'ErrorCtrl'
			}
		}
	})
	;
	$urlRouterProvider.otherwise('error');
})
