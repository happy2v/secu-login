package com.skplanet.secu.login.db.service;

import java.security.Principal;
import java.util.Enumeration;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import javax.servlet.DispatcherType;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Component;

@Component
public class DummyService {
	private final Logger logger = LogManager.getLogger(DummyService.class);
	
	 public void loggingRequest(HttpServletRequest request) {
	        String authType = request.getAuthType();
	        logger.info("authType[{}]", authType);
	        Enumeration<String> attributeNames = request.getAttributeNames();

	        if (attributeNames != null) {
	            while (attributeNames.hasMoreElements()) {
	                String attributeName = attributeNames.nextElement();
	                Object attribute = request.getAttribute(attributeName);
	                if (attributeName.equals("org.springframework.core.convert.ConversionService")) {
	                    continue;
	                }
	                logger.info("attribute[{}][{}]", attributeName, attribute);
	            }
	        } else {
	            logger.info("attribute[null]");
	        }
	        String contextPath = request.getContextPath();
	        logger.info("contextPath[{}]", contextPath);
	        Cookie[] cookies = request.getCookies();
	        if (cookies != null) {
	            for (Cookie cookie : cookies) {
	                String comment = cookie.getComment();
	                String domain = cookie.getDomain();
	                int maxAge = cookie.getMaxAge();
	                String name = cookie.getName();
	                String path = cookie.getPath();
	                boolean secure = cookie.getSecure();
	                String value = cookie.getValue();
	                int version = cookie.getVersion();
	                boolean httpOnly = cookie.isHttpOnly();
	                logger.info("cookie[name:{}][value:{}][comment:{}][domain:{}][maxAge:{}][path:{}][version:{}][secure:{}][httpOnly:{}]", name, value, comment, domain, maxAge, path, version, secure, httpOnly);
	            }
	        } else {
	            logger.info("cookie[null]");
	        }

	        String characterEncoding = request.getCharacterEncoding();
	        logger.info("characterEncoding[{}]", characterEncoding);
	        int contentLength = request.getContentLength();
	        logger.info("contentLength[{}]", contentLength);
	        long contentLengthLong = request.getContentLengthLong();
	        logger.info("contentLengthLong[{}]", contentLengthLong);
	        String contentType = request.getContentType();
	        logger.info("contentType[{}]", contentType);
	        DispatcherType dispatcherType = request.getDispatcherType();
	        if (dispatcherType != null) {
	            logger.info("dispatcherType[{}]", dispatcherType.name());
	        } else {
	            logger.info("dispatcherType[null]");
	        }

	        Enumeration<String> headerNames = request.getHeaderNames();
	        if (headerNames != null) {
	            while (headerNames.hasMoreElements()) {
	                String headerName =  headerNames.nextElement();
	                String header = request.getHeader(headerName);
	                logger.info("header[{}][{}]", headerName, header);
	            }
	        } else {
	            logger.info("header[null]");
	        }
	        String localAddr = request.getLocalAddr();
	        logger.info("localAddr[{}]", localAddr);
	        Locale locale = request.getLocale();
	        if (locale != null) {
	            final String country = locale.getCountry();
	            final String displayCountry = locale.getDisplayCountry();
	            final String displayLanguage = locale.getDisplayLanguage();
	            final String displayName = locale.getDisplayName();
	            final String displayVariant = locale.getDisplayVariant();
	            final String iso3Country = locale.getISO3Country();
	            final String iso3Language = locale.getISO3Language();
	            final String language = locale.getLanguage();
	            final String variant = locale.getVariant();
	            logger.info("locale[display[country:{}][language:{}][name:{}][variant:{}]][iso[country:{}][language:{}]][default[country:{}][language:{}][variant:{}]]", displayCountry, displayLanguage, displayName, displayVariant, iso3Country, iso3Language, country, language, variant);
	        } else {
	            logger.info("locale[null]");
	        }
	        Enumeration<Locale> locales = request.getLocales();
	        if (locales != null) {
	            while (locales.hasMoreElements()) {
	                final Locale localeElement = locales.nextElement();
	                final String country = localeElement.getCountry();
	                final String displayCountry = localeElement.getDisplayCountry();
	                final String displayLanguage = localeElement.getDisplayLanguage();
	                final String displayName = localeElement.getDisplayName();
	                final String displayVariant = localeElement.getDisplayVariant();
	                final String iso3Country = localeElement.getISO3Country();
	                final String iso3Language = localeElement.getISO3Language();
	                final String language = localeElement.getLanguage();
	                final String variant = localeElement.getVariant();
	                logger.info("locales[display[country:{}][language:{}][name:{}][variant:{}]][iso[country:{}][language:{}]][default[country:{}][language:{}][variant:{}]]", displayCountry, displayLanguage, displayName, displayVariant, iso3Country, iso3Language, country, language, variant);
	            }
	        } else {
	            logger.info("locales[null]");
	        }
	        String localName = request.getLocalName();
	        logger.info("localName[{}]", localName);
	        int localPort = request.getLocalPort();
	        logger.info("localPort[{}]", localPort);
	        String method = request.getMethod();
	        logger.info("method[{}]", method);
//	        Collection<Part> parts = null;
//	        try {
//	            parts = request.getParts();
//	            for (Iterator<Part> iterator = parts.iterator(); iterator.hasNext(); ) {
//	                Part next =  iterator.next();
//	            }
//	        } catch (IOException e) {
//	            logger.info("parts[IOException]");
//	        } catch (ServletException e) {
//	            logger.info("parts[ServletException]");
//	        }
	        String pathInfo = request.getPathInfo();
	        logger.info("pathInfo[{}]", pathInfo);
	        String pathTranslated = request.getPathTranslated();
	        logger.info("pathTranslated[{}]", pathTranslated);
	        Map<String, String[]> parameterMap = request.getParameterMap();
	        if (parameterMap != null) {
	            Set<String> parameterKeys = parameterMap.keySet();
	            for (String parameterKey : parameterKeys) {
	                String[] values = parameterMap.get(parameterKey);
	                for (String value : values) {
	                    logger.info("parameterMap[{}][{}]", parameterKey, value);
	                }
	            }
	        } else {
	            logger.info("parameterMap[null]");
	        }

	        Enumeration<String> parameterNames = request.getParameterNames();
	        if (parameterNames != null) {
	            while (parameterNames.hasMoreElements()) {
	                String parameterName =  parameterNames.nextElement();
	                logger.info("parameterNames[{}]", parameterName);
	            }
	        } else {
	            logger.info("parameterNames[null]");
	        }
	        String protocol = request.getProtocol();
	        logger.info("protocal[{}]", protocol);
	        String queryString = request.getQueryString();
	        logger.info("queryString[{}]", queryString);
	        String remoteUser = request.getRemoteUser();
	        logger.info("remoteUser[{}]", remoteUser);
	        String requestedSessionId = request.getRequestedSessionId();
	        logger.info("requestedSessionId[{}]", requestedSessionId);
	        String requestURI = request.getRequestURI();
	        logger.info("requestURI[{}]", requestURI);

//	        StringBuffer requestURL = request.getRequestURL();
//	        logger.info("requestURL[{}]", requestURL.toString());
	        String remoteAddr = request.getRemoteAddr();
	        logger.info("remoteAddr[{}]", remoteAddr);
	        String remoteHost = request.getRemoteHost();
	        logger.info("remoteHost[{}]", remoteHost);
	        int remotePort = request.getRemotePort();
	        logger.info("remotePort[{}]", remotePort);
	        String servletPath = request.getServletPath();
	        logger.info("servletPath[{}]", servletPath);
	        HttpSession session = request.getSession();
	        if (session != null) {
	            Enumeration<String> attributeNames1 = session.getAttributeNames();
	            while (attributeNames1.hasMoreElements()) {
	                String sessionAttributeName = attributeNames1.nextElement();
	                Object attribute = session.getAttribute(sessionAttributeName);
	                logger.info("session attribute[{}][{}]", sessionAttributeName, attribute);
	            }
	            long creationTime = session.getCreationTime();
	            String id1 = session.getId();
	            long lastAccessedTime = session.getLastAccessedTime();
	            int maxInactiveInterval = session.getMaxInactiveInterval();
	            boolean aNew = session.isNew();
	            logger.info("session[id:{}][isNew:{}][maxInactiveInterval:{}][createTime:{}][lastAccessedTime:{}]", id1, aNew, maxInactiveInterval, creationTime, lastAccessedTime);
	        } else {
	            logger.info("session[null]");
	        }
	        String scheme = request.getScheme();
	        logger.info("scheme[{}]", scheme);
	        String serverName = request.getServerName();
	        logger.info("serverName[{}]", serverName);
	        int serverPort = request.getServerPort();
	        logger.info("serverPort[{}]", serverPort);
	        Principal userPrincipal = request.getUserPrincipal();
	        if (userPrincipal != null) {
	            String name = userPrincipal.getName();
	            logger.info("userPrincipal[{}]", name);
	        } else {
	            logger.info("userPrincipal[null]");
	        }
	    }
}
