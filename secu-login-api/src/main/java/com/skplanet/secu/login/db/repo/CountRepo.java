package com.skplanet.secu.login.db.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.skplanet.secu.login.db.entity.CountInfo;

@Repository
public interface CountRepo extends JpaRepository<CountInfo, Long> {
	CountInfo findByAttributeKey(String attributeKey);
	CountInfo findByAttribute(String attribute);
	CountInfo findByName(String name);
}
