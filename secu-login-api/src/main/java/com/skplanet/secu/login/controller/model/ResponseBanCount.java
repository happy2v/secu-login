package com.skplanet.secu.login.controller.model;

import java.util.ArrayList;
import java.util.List;

public class ResponseBanCount {

	private List<List<Long>> chartDatas = new ArrayList<>();

	public List<List<Long>> getChartDatas() {
		return chartDatas;
	}

	public void setChartDatas(List<ChartData> chartDatas) {
		this.chartDatas = new ArrayList<>();
		for (ChartData chartData : chartDatas) {
			this.chartDatas.add(chartData.getRes());
		}
	}
}