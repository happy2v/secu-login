package com.skplanet.secu.login.controller;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.skplanet.secu.login.controller.helper.Responses;
import com.skplanet.secu.login.controller.model.ResponseData;
import com.skplanet.secu.login.controller.model.SignIn;
import com.skplanet.secu.login.controller.model.SignOut;
import com.skplanet.secu.login.controller.model.SignUp;
import com.skplanet.secu.login.db.service.AccessService;
import com.skplanet.secu.login.db.service.AuthService;
import com.skplanet.secu.login.db.service.CountService;
import com.skplanet.secu.login.detection.FraudDetectionSystem;
import com.skplanet.secu.login.geo.GeoHelper;

@RestController
public class AuthController {
	private final Logger logger = LogManager.getLogger(AuthController.class);

	@Autowired
	private AuthService authService;

	@Autowired
	private CountService countService;

	@Autowired
	private AccessService accessSV;

	@Autowired
	private GeoHelper geoHelper;
	
	@Autowired
	private FraudDetectionSystem fds;

	@RequestMapping(value = "/signup", method = RequestMethod.POST)
	public ResponseEntity<ResponseData> signup(@RequestBody SignUp signup, HttpServletRequest request)
			throws IOException {
		countService.requestCounting(request);

		final String id = signup.getId();
		final String pw = signup.getPw();
		final String email = signup.getEmail();

		boolean result = authService.existUser(id);
		if (!result) {
			final String location = geoHelper.getLocation(request);
			authService.signup(id, pw, email, location);
			accessSV.getAccessInfo(id, "signup::success", request, 0);
			return new ResponseEntity<ResponseData>(HttpStatus.OK);
		} else {
			accessSV.getAccessInfo(id, "signup::conflict", request, 0);
			return new ResponseEntity<ResponseData>(HttpStatus.CONFLICT);
		}
	}

	@RequestMapping(value = "signin", method = RequestMethod.POST)
	public ResponseEntity<ResponseData> signin(@RequestBody SignIn signin, HttpServletRequest request)
			throws IOException {
		countService.requestCounting(request);

		final String id = signin.getId();
		final String passwd = signin.getPasswd();
		int count = 0;

		boolean existUser = authService.existUser(id);
		if (!existUser) {
			accessSV.getAccessInfo(signin.getId(), "signin::noExistUser", request, count);
			count++;
			int sessionScore = accessSV.getSessionScore(id);
			if (sessionScore == 1) {
				ResponseData responseData = new ResponseData();
				responseData.setScore(1);
				return new ResponseEntity<ResponseData>(responseData, HttpStatus.OK);
			} else if (sessionScore >= 4) {
				ResponseData responseData = new ResponseData();
				responseData.setScore(4);
				return new ResponseEntity<ResponseData>(responseData, HttpStatus.OK);
			}
			return new ResponseEntity<ResponseData>(HttpStatus.GONE);
		}
		if (!authService.signin(id, passwd)) {
			accessSV.getAccessInfo(signin.getId(), "signin::fail", request, count);
			count++;
			int sessionScore = accessSV.getSessionScore(id);
			if (sessionScore == 1) {
				ResponseData responseData = new ResponseData();
				responseData.setScore(1);
				return new ResponseEntity<ResponseData>(responseData, HttpStatus.OK);
			} else if (sessionScore >= 4) {
				ResponseData responseData = new ResponseData();
				responseData.setScore(4);
				return new ResponseEntity<ResponseData>(responseData, HttpStatus.OK);
			}
			return new ResponseEntity<ResponseData>(HttpStatus.UNAUTHORIZED);
		} else {
			accessSV.getAccessInfo(signin.getId(), "signin::success", request, count);
			String sessionId = authService.getSessionId(id);
			int sessionScore = accessSV.getSessionScore(id);
			if (sessionScore == 1) {
				ResponseData responseData = new ResponseData();
				responseData.setScore(1);
				return new ResponseEntity<ResponseData>(responseData, HttpStatus.OK);
			} else if (sessionScore >= 4) {
				ResponseData responseData = new ResponseData();
				responseData.setScore(4);
				return new ResponseEntity<ResponseData>(responseData, HttpStatus.OK);
			}
			return Responses.responseString(sessionId, HttpStatus.OK);
		}
	}

	@RequestMapping(value = "signout", method = RequestMethod.POST)
	public ResponseEntity<ResponseData> signout(@RequestBody SignOut signout, HttpServletRequest request) {
		countService.requestCounting(request);

		final String sessionKey = signout.getSessionKey();
		authService.signout(sessionKey);
		return new ResponseEntity<ResponseData>(HttpStatus.OK);
	}
}