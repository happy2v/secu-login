package com.skplanet.secu.login.db.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.skplanet.secu.login.db.entity.AccountInfo;
import com.skplanet.secu.login.db.repo.AccountInfoRepo;

@Component
@Transactional
public class AccountInfoService {

	@Autowired
	private AccountInfoRepo accountInfoRepo;

	public Long getBalances(String userId) {
		AccountInfo accountInfo = accountInfoRepo.findByUserId(userId);
		if (accountInfo == null) {
			accountInfo = new AccountInfo();
			accountInfo.setUserId(userId);
			accountInfo.setBalances(0L);
			accountInfoRepo.save(accountInfo);
		}
		return accountInfo.getBalances();
	}

	public Long deposit(String userId, Long amount) {
		AccountInfo accountInfo = accountInfoRepo.findByUserId(userId);
		if (accountInfo == null) {
			accountInfo = new AccountInfo();
			accountInfo.setUserId(userId);
			accountInfo.setBalances(0L);
			accountInfoRepo.save(accountInfo);
		}
		Long balances = accountInfo.getBalances();
		if (balances == null) {
			return null;
		}
		accountInfo.setBalances(balances + amount);
		return accountInfo.getBalances();
	}

	public Long withdraw(String userId, Long amount) {
		AccountInfo accountInfo = accountInfoRepo.findByUserId(userId);
		if (accountInfo == null) {
			accountInfo = new AccountInfo();
			accountInfo.setUserId(userId);
			accountInfo.setBalances(0L);
			accountInfoRepo.save(accountInfo);
		}
		Long balances = accountInfo.getBalances();
		if (balances == null) {
			return null;
		}
		accountInfo.setBalances(balances - amount);
		return accountInfo.getBalances();
	}
}
