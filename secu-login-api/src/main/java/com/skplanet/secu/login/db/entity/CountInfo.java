package com.skplanet.secu.login.db.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Table;

import com.skplanet.secu.login.db.BaseEntity;

@Entity
@Table(indexes = {@Index(columnList = "attributeKey", unique = true), @Index(columnList = "attribute")})
public class CountInfo extends BaseEntity {

	@Id
	@GeneratedValue
	private Long id;

	private String attributeKey;
	private String attribute; 
	private Long count;
	private String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getAttribute() {
		return attribute;
	}

	public void setAttribute(String attribute) {
		this.attribute = attribute;
	}

	public Long getCount() {
		return count;
	}

	public void setCount(Long count) {
		this.count = count;
	}

	public void setAttributeKey(String attributeKey) {
		this.attributeKey = attributeKey;
	}

	public String getAttributeKey() {
		return attributeKey;
	}
}
