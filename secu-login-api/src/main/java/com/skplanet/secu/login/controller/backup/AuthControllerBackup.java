package com.skplanet.secu.login.controller.backup;
//package com.skpalnet.secu.login.controller;
//
//import java.util.UUID;
//
//import javax.servlet.http.HttpServletRequest;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.http.HttpStatus;
//import org.springframework.http.ResponseEntity;
//import org.springframework.stereotype.Controller;
//import org.springframework.web.bind.annotation.RequestBody;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestMethod;
//
//import com.skpalnet.secu.login.controller.helper.Responses;
//import com.skpalnet.secu.login.controller.model.ResponseData;
//import com.skpalnet.secu.login.controller.model.SignIn;
//import com.skpalnet.secu.login.controller.model.SignUp;
//import com.skpalnet.secu.login.db.entity.UserInfo;
//import com.skpalnet.secu.login.db.repo.UserInfoRepo;
//import com.skpalnet.secu.login.db.service.AccessService;
//import org.springframework.web.bind.annotation.RestController;
//
//@RestController
//public class AuthController {
//
//	@Autowired
//	private UserInfoRepo userInfoRepo;
//
//	@Autowired
//	private AccessService accessSV;
//
//	@RequestMapping("/")
//	public String HomeController() {
//		return "redirect:/access";
//	}
//
//	@RequestMapping(value = "/signin", method = RequestMethod.POST)
//	public ResponseEntity<ResponseData> signin(@RequestBody SignIn signin, HttpServletRequest request) {
//		final String id = signin.getId();
//		final String passwd = signin.getPasswd();
//		// final String info = signin.getInfo();
//		// final StringBuilder sb = new StringBuilder();
//
//		final UserInfo user = userInfoRepo.findByUserId(id);
//		if (user == null) {
//			accessSV.getAccessInfo(signin.getId(), "noExistUser", request);
//			accessSV.updateTotalTable();
//			accessSV.printProbability(request);
//			return Responses.response(HttpStatus.BAD_REQUEST);
//		}
//		else {
//			if (user.getPassword().equals(passwd)) {
//				accessSV.getAccessInfo(signin.getId(), "success", request);
//				accessSV.updateTotalTable();
//				accessSV.printProbability(request);
//				return Responses.responseString(id, HttpStatus.OK);
//			}
//			else {
//				accessSV.getAccessInfo(signin.getId(), "fail", request);
//				accessSV.updateTotalTable();
//				accessSV.printProbability(request);
//				return Responses.responseString("try again",HttpStatus.FORBIDDEN);
//			}
//		}
//	}
//
//	@RequestMapping(value = "/signup", method = RequestMethod.POST)
//	// public ResponseEntity<String> signup(@RequestBody SignUp signup) {
//	public ResponseEntity<ResponseData> signup(@RequestBody SignUp signup) {
//		final String id = signup.getId();
//		final String pw = signup.getPw();
//		System.out.println(id);
//		// @Nullable
//		// final UserInfo user = userInfoRepo.findByUserId(id);
//		// String state = null;
//		if (userInfoRepo.findByUserId(id) == null) {
//			final String uuid = UUID.randomUUID().toString();
//			final UserInfo userInfo = new UserInfo();
//			userInfo.setUserId(id);
//			userInfo.setPassword(pw);
//			userInfo.setAuthKey(uuid);
//			userInfoRepo.save(userInfo);
//			return Responses.responseString(uuid, HttpStatus.OK);
////			return "redirect:/access?ID=" + id + "&state=success";
//		}
//		return new ResponseEntity<ResponseData>(HttpStatus.CONFLICT);
////		return "redirect:/access?ID=" + id + "&state=fail";
//	}
//
//	@RequestMapping("/success")
//	public ResponseEntity<String> ok() {
//		// System.out.println("ID: "+ value +"\nPW: "+valuepw+"\n");
//		return new ResponseEntity<String>("!!!successful signup!!!", HttpStatus.OK);
//	}
//
//	@RequestMapping("/fail")
//	public ResponseEntity<String> fail() {
//		return new ResponseEntity<String>("!!!fail page already exist!!!", HttpStatus.OK);
//	}
//}
