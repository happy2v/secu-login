package com.skplanet.secu.login;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.skplanet.secu.login.cache.BanCountCache;
import com.skplanet.secu.login.db.service.DashboardService;

@Component
public class DashboardScheduler {

	@Autowired
    private DashboardService dashboardService;
	
	@Autowired
	private BanCountCache banCountCache;
    
    @Scheduled(fixedDelay = 10000)
    public void updateCount() {
    	Integer banCount = dashboardService.getBanCount(10);
    	banCountCache.put(banCount);
    }
}
