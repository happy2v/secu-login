package com.skplanet.secu.login.controller.model;

import javax.validation.constraints.NotNull;

/**
 * Created by happy2v on 2016. 2. 14..
 */
public class SignOut {
    @NotNull
    private String sessionKey;

    public void setSessionKey(String sessionKey) {
        this.sessionKey = sessionKey;
    }

    public String getSessionKey() {
        return sessionKey;
    }
}
