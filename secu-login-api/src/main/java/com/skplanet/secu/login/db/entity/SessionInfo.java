package com.skplanet.secu.login.db.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Lob;
import javax.persistence.Table;

import com.skplanet.secu.login.db.BaseEntity;

@Entity
@Table(indexes = {@Index(columnList = "userId", unique=false), @Index(columnList = "sessionKey", unique=true)})
public class SessionInfo extends BaseEntity {

	@Id
	@GeneratedValue
	private Long id;

	private String userId;
	private String sessionKey;
	private String state;
	private String stateOfTrial;
	private String accessCountry;
	private int FDSscore;
	
	public int getFDSscore() {
		return FDSscore;
	}

	public void setFDSscore(int fDSscore) {
		FDSscore = fDSscore;
	}

	public String getAccessCountry() {
		return accessCountry;
	}

	public void setAccessCountry(String accessCountry) {
		this.accessCountry = accessCountry;
	}

	@Lob
	private String header;
	
	public String getStateOfTrial() {
		return stateOfTrial;
	}

	public void setStateOfTrial(String stateOfTrial) {
		this.stateOfTrial = stateOfTrial;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getSessionKey() {
		return sessionKey;
	}

	public void setSessionKey(String sessionKey) {
		this.sessionKey = sessionKey;
	}

	public String getHeader() {
		return header;
	}

	public void setHeader(String header) {
		this.header = header;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

}
