package com.skplanet.secu.login.db;

import java.util.Date;

import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;

public class BaseEntityListener {

	@PrePersist
	public void prePersist(BaseEntity baseEntity) {
		final Date date = new Date();
		baseEntity.setCreatedDate(date);
		baseEntity.setUpdatedDate(date);
	}
	
	@PreUpdate
	public void preUpdate(BaseEntity baseEntity) {
		baseEntity.setUpdatedDate(new Date());
	}
}
