package com.skplanet.secu.login.geo;

import com.google.common.collect.Maps;
import java.util.Map;

/*public class GeoLocation {
    private static final Map<Coordination, GeoLocation> geoLocationCache = Maps.newConcurrentMap();

    private final String country;
    private final String stateProv;
    private final String city;
    private final Coordination coord;

    private GeoLocation(String country, String stateProv, String city, Coordination coord) {
        this.country = country;
        this.stateProv = stateProv;
        this.city = city;
        this.coord = coord;
    }

    public static GeoLocation of(String country, String stateProv, String city, Coordination coord) {
        if (geoLocationCache.containsKey(coord)) {
            return geoLocationCache.get(coord);
        }
        GeoLocation newLocation = new GeoLocation(country, stateProv, city, coord);
        geoLocationCache.put(coord, newLocation);

        return newLocation;
    }

    public String getCountry() {
        return country;
    }

    public String getStateProv() {
        return stateProv;
    }

    public String getCity() {
        return city;
    }

    public Coordination getCoord() {
        return coord;
    }
}
*/
public class GeoLocation {
	private static final Map<Coordination, GeoLocation> geoLocationCache = Maps.newConcurrentMap();

	private final String country;
	// private final String stateProv;
	private final String fullCountry;
	private final Coordination coord;

	private GeoLocation(String country, String fullCountry, Coordination coord) {
		this.country = country;
		// this.stateProv = stateProv;
		this.fullCountry = fullCountry;
		this.coord = coord;
	}

	public static GeoLocation of(String country, String fullCountry, Coordination coord) {
		if (geoLocationCache.containsKey(coord)) {
			return geoLocationCache.get(coord);
		}
		GeoLocation newLocation = new GeoLocation(country, fullCountry, coord);
		geoLocationCache.put(coord, newLocation);

		return newLocation;
	}

	public String getCountry() {
		return country;
	}

	public String getFullCountry() {
		return fullCountry;
	}

	public Coordination getCoord() {
		return coord;
	}
}
