package com.skplanet.secu.login.db.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.skplanet.secu.login.db.entity.UserInfo;

@Repository
public interface UserInfoRepo extends JpaRepository<UserInfo, Long> {
	UserInfo findByUserId(String userId);
}
