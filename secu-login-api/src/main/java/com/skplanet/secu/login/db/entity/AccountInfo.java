package com.skplanet.secu.login.db.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class AccountInfo {

	@Id
	@GeneratedValue
	private Long id;
	private String userId;
	private Long balances;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public Long getBalances() {
		return balances;
	}

	public void setBalances(Long balances) {
		this.balances = balances;
	}
}
