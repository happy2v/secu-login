package com.skplanet.secu.login.controller.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

public class ResponseBanReason {
	private List<PieChartData> reasons = new ArrayList<>();

	public List<PieChartData> getReasons() {
		return reasons;
	}

	public void setReasons(HashMap<String, Integer> hashMap) {
		reasons = new ArrayList<>();
		Set<String> keySet = hashMap.keySet();
		for (String key : keySet) {
			Integer value = hashMap.get(key);
			PieChartData pieChartData = new PieChartData();
			pieChartData.setData(Long.valueOf(value));
			pieChartData.setLabel(key);
			reasons.add(pieChartData);
		}
	}
}
