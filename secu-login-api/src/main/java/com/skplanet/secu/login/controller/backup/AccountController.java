package com.skplanet.secu.login.controller.backup;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.skplanet.secu.login.controller.model.AuthKey;
import com.skplanet.secu.login.controller.model.DepositInfo;
import com.skplanet.secu.login.controller.model.WithdrawInfo;
import com.skplanet.secu.login.db.service.AccountInfoService;
import com.skplanet.secu.login.db.service.AuthService;

@Controller
public class AccountController {
	
	@Autowired
	private AccountInfoService accountSvc;
	
	@Autowired	
	private AuthService authService;

	@RequestMapping(value = "/account", method = RequestMethod.GET)
	public ResponseEntity<Long> getBalances(@RequestBody AuthKey authKey) {
		
		String userId = authService.getUserId(authKey.getAuthKey());
		if (userId == null) {
			return new ResponseEntity<Long>(HttpStatus.BAD_REQUEST);
		}
		
		Long balances = accountSvc.getBalances(userId);
		return new ResponseEntity<Long>(balances, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/deposit", method = RequestMethod.POST)
	public ResponseEntity<Long> deposit(@RequestBody DepositInfo depositInfo) {
		final String authKey = depositInfo.getAuthKey();
		final Long amount = depositInfo.getAmount();
		
		String userId = authService.getUserId(authKey);
		if (userId == null) {
			return new ResponseEntity<Long>(HttpStatus.BAD_REQUEST);
		}
		
		Long balances = accountSvc.deposit(userId, amount);
		return new ResponseEntity<Long>(balances, HttpStatus.OK);
	}
	
	
	@RequestMapping(value = "/withdraw", method = RequestMethod.POST)
	public ResponseEntity<Long> withdraw(@RequestBody WithdrawInfo withdraw) {
		final String authKey = withdraw.getAuthKey();
		final Long amount = withdraw.getAmount();
		
		String userId = authService.getUserId(authKey);
		if (userId == null) {
			return new ResponseEntity<Long>(HttpStatus.BAD_REQUEST);
		}
		
		Long balances = accountSvc.deposit(userId, amount);
		return new ResponseEntity<Long>(balances, HttpStatus.OK);
	}
}
