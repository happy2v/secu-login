package com.skplanet.secu.login.controller.model;

public class PieChartData {

	private String label;
	private Long data;

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public Long getData() {
		return data;
	}

	public void setData(Long data) {
		this.data = data;
	}
}
