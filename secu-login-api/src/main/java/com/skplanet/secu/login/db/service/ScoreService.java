package com.skplanet.secu.login.db.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.skplanet.secu.login.db.entity.ScoreInfo;
import com.skplanet.secu.login.db.repo.ScoreRepo;

@Component
@Transactional(readOnly = false)
public class ScoreService {
	
	@Autowired
	private ScoreRepo scoreRepo;
	
	public void addScore(String userId, String reason, int point) {
		ScoreInfo scoreInfo = new ScoreInfo();
		scoreInfo.setPoint(point);
		scoreInfo.setReason(reason);
		scoreInfo.setUserId(userId);
		scoreRepo.save(scoreInfo);
	}

	@Transactional(readOnly = true)
	public List<ScoreInfo> getAllScore() {
		return scoreRepo.findAll();
	}
	
	@Transactional(readOnly = true)
	public Page<ScoreInfo> getAllScore(Pageable pageable) {
		return scoreRepo.findAll(pageable);
	}
}
