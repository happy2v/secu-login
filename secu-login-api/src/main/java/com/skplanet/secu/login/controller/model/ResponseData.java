package com.skplanet.secu.login.controller.model;

public class ResponseData {

	private String data;
	private int score;

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	public int getScore() {
		return score;
	}

	public void setScore(int score) {
		this.score = score;
	}
}
