package com.skplanet.secu.login.db.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Table;

import com.skplanet.secu.login.db.BaseEntity;

@Entity
@Table(indexes = {@Index(columnList = "infoName")})
public class TotalInfo extends BaseEntity {
	
	@Id
	@GeneratedValue
	private Long id;
	
	private String infoName;
	private Long totalCount;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getInfoName() {
		return infoName;
	}
	public void setInfoName(String infoName) {
		this.infoName = infoName;
	}
	public Long getTotalCount() {
		return totalCount;
	}
	public void setTotalCount(Long totalCount) {
		this.totalCount = totalCount;
	}
	
}
