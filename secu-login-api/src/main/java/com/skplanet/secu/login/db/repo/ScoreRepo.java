package com.skplanet.secu.login.db.repo;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.skplanet.secu.login.db.entity.ScoreInfo;
import com.skplanet.secu.login.db.entity.SessionInfo;

@Repository
public interface ScoreRepo extends JpaRepository<ScoreInfo, Long> {

	List<ScoreInfo> findByCreatedDateAfter(Date date);
}
