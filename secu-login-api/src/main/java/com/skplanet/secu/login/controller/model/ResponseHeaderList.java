package com.skplanet.secu.login.controller.model;

import java.util.ArrayList;
import java.util.List;

public class ResponseHeaderList {

	private List<HeaderRatio> headerRatios = new ArrayList<>();

	public List<HeaderRatio> getHeaderRatios() {
		return headerRatios;
	}

	public void setHeaderRatios(List<HeaderRatio> headerRatios) {
		this.headerRatios = headerRatios;
	}
}
