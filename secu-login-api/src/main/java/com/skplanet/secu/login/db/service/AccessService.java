package com.skplanet.secu.login.db.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.StringTokenizer;
import java.util.UUID;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

import com.skplanet.secu.login.db.entity.*;
import com.skplanet.secu.login.db.repo.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.skplanet.secu.login.detection.FraudDetectionSystem;
import com.skplanet.secu.login.geo.GeoHelper;

//import org.apache.http.impl.client.DefaultConnectionKeepAliveStrategy;

@Component
@Transactional
public class AccessService {

	@Autowired
	private SessionRepo sessionRepo;

	@Autowired
	private UserInfoRepo userInfoRepo;

	@Autowired
	private CountRepo countRepo;

	@Autowired
	private TotalRepo totalRepo;

	@Autowired
	private GeoHelper geoHelper;

	@Autowired
	private ScoreRepo scoreRepo;

	@Autowired
	private FraudDetectionSystem fds;

	//
	// @Autowired
	// private CountInfo countIF;
	//
	public List<String> parsingHeader(String sessionKey) {
		SessionInfo sessioninfo = sessionRepo.findBySessionKey(sessionKey);
		List<String> toreturn = new ArrayList<String>();
		if(sessioninfo == null){
			System.out.println("null error");
		}
		else{
			String header = sessioninfo.getHeader();
			StringTokenizer st = new StringTokenizer(header, "#");
			List<String> headerL = new ArrayList<String>();
			while (st.hasMoreTokens()) {
				headerL.add(st.nextToken());
			}
			
			int j=0;
			for (int i = 0; i < headerL.size(); i++) {
				//System.out.println(headerL.get(i).toString() + "\n");
				if(headerL.get(i).toString().equals("accept")|headerL.get(i).toString().equals("user-agent")|headerL.get(i).toString().equals("content-type")|headerL.get(i).toString().equals("accept-encoding")|headerL.get(i).toString().equals("accept-language")|headerL.get(i).toString().equals("user ip")){
					toreturn.add(headerL.get(i+1).toString());
					j++;
				}
			}
		}
		return toreturn; 
	}

	public String getAccessInfo(String id, String stateofTrial, HttpServletRequest request, int trialNum)
			throws IOException {
		Enumeration<String> headerNames = request.getHeaderNames();
		final StringBuilder sb = new StringBuilder();
		String headerArray = "";
		String copyHeader = "";
		while (headerNames.hasMoreElements()) {
			String nextElement = headerNames.nextElement();
			String header = request.getHeader(nextElement);
			sb.append(nextElement);
			sb.append(":");
			sb.append(header);
			countAccess(header, nextElement);
			sb.append("<br>");
			headerArray = headerArray + nextElement + " # " + header + " # ";
			copyHeader = copyHeader + nextElement + " # " + header + " # ";
		}

		Cookie[] cookie = request.getCookies();
		sb.append("cookie : " + cookie + "<br>");
		headerArray = headerArray + "cookie # " + cookie + " # ";

		String userIp = request.getRemoteAddr();
		sb.append("user IP : " + userIp + "<br>");
		headerArray = headerArray + "user IP # " + userIp + " # ";
		countAccess(userIp, "user ip");

		String clientName = request.getRemoteHost();
		sb.append("client Name : " + clientName + "<br>");
		headerArray = headerArray + "client Name # " + clientName + " # ";

		int clientPort = request.getRemotePort();
		sb.append("client port number : " + clientPort + "<br>");
		headerArray = headerArray + "client port number # " + clientPort + " # ";

		countAccess(String.valueOf(clientPort), "port number");


		// final String userId = sessionInfo.getUserId();
		final String userId = id;
		final String sessionUuid = UUID.randomUUID().toString();

		SessionInfo sessionInfo = new SessionInfo();
		final UserInfo user = userInfoRepo.findByUserId(userId);// userid
		if (user != null) {
			String uId = user.getUserId();
			sessionInfo.setUserId(uId);
		}
		sessionInfo.setSessionKey(sessionUuid);// session id
		sessionInfo.setHeader(headerArray);// header info
		sessionInfo.setStateOfTrial(stateofTrial);
		final String country = geoHelper.getLocation(userIp);
		if (country != null) {
			sessionInfo.setAccessCountry(country);
		}
		sessionRepo.save(sessionInfo);
		callFDS(trialNum, sessionUuid, userId, headerArray);
		return country;
	}

	public void callFDS(int trialNum, String sessionUuid, String userId, String headerArray) {
		fds.compareLocation(userId, sessionUuid);
//		if (trialNum >= 5) /* number of login trial */ {
//			fds.fiveTrials(sessionUuid);
//		}
		fds.anomalyAccess(headerArray, sessionUuid);
	}

	public void countAccess(String valueToCount, String name) {
		String str = "";
		str = name+valueToCount;
		CountInfo countIF = countRepo.findByAttributeKey(str);
		if (countIF == null) {
			countIF = new CountInfo();
			countIF.setAttribute(valueToCount);
			countIF.setAttributeKey(str);
			countIF.setName(name);
			countIF.setCount((long) 1);
			System.out.println("no exist: "+ str + "was made now");
		} else
			countIF.setCount(countIF.getCount() + 1);
		countRepo.save(countIF);
	}

	// public void countTotal(String totalCountName) {
	//
	// long total = 0;
	// TotalInfo totalIF = totalRepo.findByInfoName(totalCountName);
	// long count = countRepo.count();
	// for (long i = 1; i < count + 1; i++) {
	// CountInfo toFind = countRepo.findOne(i);
	// if (toFind == null) {
	// System.out.println("toFind is Null : " + i + ", " + totalCountName);
	// } else if (toFind.getName().equals(totalCountName)) {
	// total = total +toFind.getCount();
	// }
	// }
	// if(totalIF == null)
	// totalIF = new TotalInfo();
	// totalIF.setInfoName(totalCountName);
	// totalIF.setTotalCount(total);
	// totalRepo.save(totalIF);
	// }
	//
	// public void updateTotalTable() {
	// countTotal("host");
	// countTotal("connection");
	// countTotal("accept");
	// countTotal("user-agent");
	// countTotal("referer");
	// countTotal("accept-encoding");
	// countTotal("accept-language");
	// countTotal("cookie");
	// countTotal("user ip");
	// countTotal("port number");
	// countTotal("content-type");
	// countTotal("dnt");
	// countTotal("content-length");
	// countTotal("origin");
	// }

	public double getProbability(String wantToKnow) {
		CountInfo countIF = countRepo.findByAttribute(wantToKnow);
		TotalInfo totalIF = totalRepo.findByInfoName(countIF.getName());
		double probability = (double) countIF.getCount() / totalIF.getTotalCount() * 100;
		return probability;
	}

	public void printProbability(HttpServletRequest request) {
		// updateTotalTable();
		Enumeration<String> headerNames2 = request.getHeaderNames();
		int count = 0;
		double total = 0;
		while (headerNames2.hasMoreElements()) {
			String nextElement = headerNames2.nextElement();
			String header = request.getHeader(nextElement);
			double getpro = getProbability(header);
			System.out.println(header + " >> " + getpro + "%\n");
			count = count + 1;
			total = total + getpro;
		}
		String userIp = request.getRemoteAddr();
		double getpro = getProbability(userIp);
		System.out.println(userIp + " >> " + getpro + "%\n");
		total = total + getpro;
		count = count + 1;
		int clientPort = request.getRemotePort();
		getpro = getProbability(String.valueOf(clientPort));
		System.out.println(String.valueOf(clientPort) + " >> " + getpro + "%\n");
		total = total + getpro;
		count = count + 1;
		double totalProb = total / count;
		System.out.println("Uniqueness >> " + totalProb + "%\n");
	}

	public int getSessionScore(String userId) {
		List<ScoreInfo> all = scoreRepo.findAll();
		int score = 0;
		for (ScoreInfo scoreInfo : all) {
			if (scoreInfo.getUserId() != null && scoreInfo.getUserId().equals(userId)) {
				score = score + scoreInfo.getPoint();
			}
		}
		return score;
	}
}
