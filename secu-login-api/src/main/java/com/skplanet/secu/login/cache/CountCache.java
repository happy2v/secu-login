package com.skplanet.secu.login.cache;

import java.util.HashMap;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Component;

@Component
public class CountCache {
	private final Logger logger = LogManager.getLogger(CountCache.class);

	private HashMap<String, String> valueData = new HashMap<String, String>();
	private HashMap<String, String> valueName = new HashMap<String, String>();
	private ConcurrentHashMap<String, AtomicLong> valueCount = new ConcurrentHashMap<String, AtomicLong>();
	private ConcurrentHashMap<String, AtomicLong> keyCount = new ConcurrentHashMap<String, AtomicLong>();
	private ConcurrentHashMap<String, ConcurrentHashMap<String, AtomicLong>> keyValueCount = new ConcurrentHashMap<>();

	public void setValueCount(String key, String value, Long count) {
		initValueCount(key, value, count);
		
		initKeyCount(key, value, count);
	}

	private void initKeyCount(String key, String value, Long count) {
		if (!keyValueCount.containsKey(key)) {
			final AtomicLong valueCount = new AtomicLong();
			valueCount.set(count);
			final ConcurrentHashMap<String, AtomicLong> valueMap = new ConcurrentHashMap<>();
			valueMap.put(value, valueCount);
			keyValueCount.put(key, valueMap);
		} else {
			final ConcurrentHashMap<String, AtomicLong> valueMap = keyValueCount.get(key);
			if (!valueMap.containsKey(value)) {
				final AtomicLong valueCount = new AtomicLong();
				valueCount.set(count);
				valueMap.put(value, valueCount);
			}
		}
	}



	private void initValueCount(String key, String value, Long count) {
		AtomicLong atomicLong = valueCount.get(key + value);
		if (atomicLong == null) {			
			final AtomicLong atomicValueCount = new AtomicLong();
			atomicValueCount.set(count);
			valueCount.put(key + value, atomicValueCount);

			if (!valueData.containsKey(key + value)) {
				valueData.put(key + value, value);
			}
			if (!valueName.containsKey(key + value)) {
				valueName.put(key + value, key);
			}
		} else {
			atomicLong.incrementAndGet();
		}
		
	}
	
	

	public void setKeyCount(String key, Long count) {
		final AtomicLong atomicKeyCount = new AtomicLong();
		atomicKeyCount.set(count);
		keyCount.put(key, atomicKeyCount);
	}

	public boolean containsValue(String key, String value) {
		return valueCount.containsKey(key + value);
	}

	public boolean containsKey(String key) {
		return keyCount.containsKey(key);
	}

	public void count(String key, String value) {
		final AtomicLong atomicValue = valueCount.get(key + value);
		if (atomicValue != null) {
			atomicValue.incrementAndGet();
		} else {
			initValueCount(key, value, 1L);
		}

		final AtomicLong atomicKey = keyCount.get(key);
		if (atomicKey != null) {
			atomicKey.incrementAndGet();
		} else {
			initKeyCount(key, value, 1L);
		}
		
		ConcurrentHashMap<String, AtomicLong> valueMap = keyValueCount.get(key);
		if (valueMap != null) {
			AtomicLong atomicLong = valueMap.get(value);
			if (atomicLong != null) {
				atomicLong.incrementAndGet();
			} else {
				atomicLong = new AtomicLong();
				atomicLong.set(1L);
				valueMap.put(value, atomicLong);
			}
		} else {
			final AtomicLong valueCount = new AtomicLong();
			valueCount.set(1L);
			valueMap = new ConcurrentHashMap<>();
			valueMap.put(value, valueCount);
			keyValueCount.put(key, valueMap);
		}
	}

	public void printCount() {
		for (String value : valueData.keySet()) {
			String key = valueData.get(value);
			AtomicLong atomicKeyCount = keyCount.get(key);
			AtomicLong atomicValueCount = valueCount.get(value);
			logger.info("Count[{}:{}][{}:{}]", key, atomicKeyCount.get(), value, atomicValueCount.get());
		}
	}

	public Long getTotalCount(String key) {
		AtomicLong atomicLong = keyCount.get(key);
		if (atomicLong != null) {			
			return atomicLong.get();
		}
		return 0L;
	}

	public Long getValueCount(String key, String value) {
		AtomicLong atomicLong = valueCount.get(key + value);
		if (atomicLong != null) {			
			return atomicLong.get();
		}
		return 0L;
	}

	public ConcurrentHashMap<String, AtomicLong> getValueCount() {
		return valueCount;
	}

	public ConcurrentHashMap<String, AtomicLong> getKeyCount() {
		return keyCount;
	}

	public HashMap<String, String> getValueData() {
		return valueData;
	}

	public HashMap<String, String> getValueName() {
		return valueName;
	}
	
	public ConcurrentHashMap<String, ConcurrentHashMap<String, AtomicLong>> getKeyValueCount() {
		return keyValueCount;
	}
}
