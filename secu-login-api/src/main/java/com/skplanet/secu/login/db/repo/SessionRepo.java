package com.skplanet.secu.login.db.repo;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.skplanet.secu.login.db.entity.SessionInfo;

@Repository
public interface SessionRepo extends JpaRepository<SessionInfo, Long> {
	SessionInfo findBySessionKey(String sessionKey);
	List<SessionInfo> findByUserId(String userId);
	List<SessionInfo> findByCreatedDateAfter(Date date);
}
