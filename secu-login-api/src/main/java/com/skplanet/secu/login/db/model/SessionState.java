package com.skplanet.secu.login.db.model;

/**
 * Created by happy2v on 2016. 2. 14..
 */
public enum SessionState {
    ACTIVE,
    EXPIRED
}
