package com.skplanet.secu.login.db.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.skplanet.secu.login.db.entity.TotalInfo;

@Repository
public interface TotalRepo extends JpaRepository<TotalInfo, Long>{
	TotalInfo findByInfoName(String infoName);
}
