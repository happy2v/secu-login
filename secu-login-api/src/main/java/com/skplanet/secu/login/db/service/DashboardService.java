package com.skplanet.secu.login.db.service;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.skplanet.secu.login.db.entity.ScoreInfo;
import com.skplanet.secu.login.db.entity.SessionInfo;
import com.skplanet.secu.login.db.repo.ScoreRepo;
import com.skplanet.secu.login.db.repo.SessionRepo;

@Component
@Transactional(readOnly = true)
public class DashboardService {

	@Autowired
	private SessionRepo sessionRepo;
	
	@Autowired
	private ScoreRepo scoreRepo;
	
	public HashMap<String, Integer> getBanReason() {
		HashMap<String, Integer> banReasonMap = new HashMap<>();
		final List<SessionInfo> sessionInfos = sessionRepo.findAll();
		for (SessionInfo sessionInfo : sessionInfos) {
			String stateOfTrial = sessionInfo.getStateOfTrial();
			if (stateOfTrial == null) {
				continue;
			}
			if (stateOfTrial.equals("signin::success")) {
				continue;
			}
			if (stateOfTrial.equals("signup::success")) {
				continue;
			}
			Integer integer = banReasonMap.get(stateOfTrial);
			if (integer == null) {
				integer = 0;
			}
			integer++;
			banReasonMap.put(stateOfTrial, integer);
		}
		return banReasonMap;
	}
	
	public Integer getBanCount(int minutes) {
//		int count = 0;
//		long millis = TimeUnit.SECONDS.toMillis(minutes);
//		Date date = new Date(System.currentTimeMillis() - millis);
//		final List<SessionInfo> sessionInfos = sessionRepo.findByCreatedDateAfter(date);
//		for (SessionInfo sessionInfo : sessionInfos) {
//			String stateOfTrial = sessionInfo.getStateOfTrial();
//			if (!stateOfTrial.equals("signin::success") && !stateOfTrial.equals("signup::success")) {
//				count++;
//			}
//		}
		
		int count = 0;
		long millis = TimeUnit.SECONDS.toMillis(minutes);
		Date date = new Date(System.currentTimeMillis() - millis);
		List<ScoreInfo> scoreInfos = scoreRepo.findByCreatedDateAfter(date);
		for (ScoreInfo scoreInfo : scoreInfos) {
			count += scoreInfo.getPoint();
		}
		
		return count;
	}
}
