package com.skplanet.secu.login.controller.model;

public class TestReq {

	private String data;

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	@Override
	public String toString() {
		return "TestReq [data=" + data + "]";
	}
}
