package com.skplanet.secu.login.geo;

import java.io.*;
import java.math.BigInteger;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Map;
import java.util.NavigableMap;
import java.util.concurrent.ConcurrentSkipListMap;

import com.skplanet.secu.login.geo.IP4Converter;

public class IPReader {
	private final NavigableMap<Long, CSV> ipLookup;

	private IPReader(NavigableMap<Long, CSV> ipLookup) {
		this.ipLookup = ipLookup;
	}

	public static IPReader build(String dbPath) throws IOException {
		File ipdb = new File(dbPath);
		InputStream inputStream = new FileInputStream(ipdb);
		return build(inputStream);
	}

	public static IPReader build(InputStream inputStream) throws IOException {
		NavigableMap<Long, CSV> lookup = new ConcurrentSkipListMap<Long, CSV>();

		BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
		while (reader.ready()) {
			CSV csv = CSV.parse(reader.readLine());
			lookup.put(IP4Converter.toLong(csv.getIpStart()), // csv.getIpStart()
					csv);
		}
		reader.close();

		return new IPReader(lookup);
	}

	public GeoLocation lookup(String ipAddress) throws UnknownHostException {
		InetAddress inetAddress = InetAddress.getByName(ipAddress);
		System.out.println(inetAddress);
		return lookup(inetAddress);
	}

	public GeoLocation lookup(InetAddress inetAddress) throws UnknownHostException {

		return lookup(IP4Converter.toLong(inetAddress.getAddress()));
	}

	private GeoLocation lookup(Long address) throws UnknownHostException {
		Map.Entry<Long, CSV> entry = ipLookup.lowerEntry(address);
		if (entry == null) {
			return null;
		}

		CSV csv = entry.getValue();
		if (IP4Converter.toLong(csv.getIpEnd()) < address) {
			return null;
		}

		return GeoLocation.of(csv.getCountry(), csv.getFullCountry(),
				Coordination.of(Double.parseDouble(String.valueOf(csv.getLatitude())),
						Double.parseDouble(String.valueOf(csv.getLongitude())))); /// Coordination.of(csv.getLatitude(),
																					/// csv.getLongitude())
	}

	private Long toLong(byte[] address) {
		return new BigInteger(address).longValue();
	}
}
