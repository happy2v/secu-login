package com.skplanet.secu.login.controller.model;

public class SignIn {

	private String id;
	private String passwd;
	// private String info;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getPasswd() {
		return passwd;
	}

	public void setPasswd(String passwd) {
		this.passwd = passwd;
	}

	/*
	 * public String getInfo() { return info; }
	 * 
	 * public void setInfo(String info) { this.info = info; }
	 */
}
