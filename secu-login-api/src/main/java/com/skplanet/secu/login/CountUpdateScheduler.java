package com.skplanet.secu.login;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.skplanet.secu.login.db.service.CountService;

@Component
public class CountUpdateScheduler {

    @Autowired
    private CountService countService;
    
    private static boolean isFirst = true;

    @Scheduled(fixedDelay = 60000)
    public void updateCount() {
    	if (isFirst) {
    		countService.initCount();
    		isFirst = false;
    	} else {    		
    		countService.syncCount();
    	}
    }
}