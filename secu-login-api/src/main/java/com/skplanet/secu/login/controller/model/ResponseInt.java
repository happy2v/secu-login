package com.skplanet.secu.login.controller.model;

public class ResponseInt {

	private Integer data;

	public Integer getData() {
		return data;
	}

	public void setData(Integer data) {
		this.data = data;
	}
}
