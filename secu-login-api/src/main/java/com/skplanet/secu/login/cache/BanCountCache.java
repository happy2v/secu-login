package com.skplanet.secu.login.cache;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.skplanet.secu.login.controller.model.ChartData;

@Component
public class BanCountCache {

	private List<ChartData> banData = new ArrayList<>();
	
	public void put(int count) {
		if (banData.size() >= 120) {
			banData.remove(0);
		}
		ChartData chartData = new ChartData();
		List<Long> res = new ArrayList<>();
		res.add(System.currentTimeMillis());
		res.add(Long.valueOf(count));
		chartData.setRes(res);
		banData.add(chartData);
	}
	
	public List<ChartData> getAll() {
		return banData;
	}

	public void putPre(long oldTime, int i) {
		ChartData chartData = new ChartData();
		List<Long> res = new ArrayList<>();
		res.add(System.currentTimeMillis());
		res.add(Long.valueOf(i));
		chartData.setRes(res);
		banData.add(0, chartData);
	}
}