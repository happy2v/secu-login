package com.skplanet.secu.login.controller.model;

import java.util.ArrayList;
import java.util.List;

public class ChartData {

	private List<Long> res = new ArrayList<>();

	public List<Long> getRes() {
		return res;
	}

	public void setRes(List<Long> res) {
		this.res = res;
	}
}