package com.skplanet.secu.login.db.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.skplanet.secu.login.db.entity.AccountInfo;

@Repository
public interface AccountInfoRepo extends JpaRepository<AccountInfo, Long> {
	AccountInfo findByUserId(String userId);
}
