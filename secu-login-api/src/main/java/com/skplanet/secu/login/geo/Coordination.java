package com.skplanet.secu.login.geo;

public class Coordination {
	private final Double latitude;
	private final Double longitude;

	private Coordination(Double lat, Double lon) {
		this.latitude = lat;
		this.longitude = lon;
	}

	public static Coordination of(Double lat, Double lon) {
		return new Coordination(lat, lon);
	}

	public Double getLongitude() {
		return longitude;
	}

	public Double getLatitude() {
		return latitude;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;

		Coordination that = (Coordination) o;
		return latitude.equals(that.getLatitude()) && longitude.equals(that.longitude);
	}

	@Override
	public int hashCode() {
		int result = latitude.hashCode();
		result = 31 * result + longitude.hashCode();
		return result;
	}
}
