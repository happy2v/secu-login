package com.skplanet.secu.login.controller.model;

import javax.validation.constraints.NotNull;

public class WithdrawInfo {

	@NotNull
	private String authKey;
	private Long amount;

	public String getAuthKey() {
		return authKey;
	}

	public void setAuthKey(String authKey) {
		this.authKey = authKey;
	}

	public Long getAmount() {
		return amount;
	}

	public void setAmount(Long amount) {
		this.amount = amount;
	}

}
