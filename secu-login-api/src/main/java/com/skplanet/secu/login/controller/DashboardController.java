package com.skplanet.secu.login.controller;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.servlet.http.HttpServletRequest;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.skplanet.secu.login.cache.BanCountCache;
import com.skplanet.secu.login.controller.model.ChartData;
import com.skplanet.secu.login.controller.model.HeaderRatio;
import com.skplanet.secu.login.controller.model.PieChartData;
import com.skplanet.secu.login.controller.model.ResponseBanCount;
import com.skplanet.secu.login.controller.model.ResponseBanReason;
import com.skplanet.secu.login.controller.model.ResponseHeaderList;
import com.skplanet.secu.login.db.entity.ScoreInfo;
import com.skplanet.secu.login.db.service.CountService;
import com.skplanet.secu.login.db.service.DashboardService;
import com.skplanet.secu.login.db.service.ScoreService;

@RestController
public class DashboardController {
	private final Logger logger = LogManager.getLogger(DashboardController.class);

	@Autowired
	private CountService countService;
	
	@Autowired
	private ScoreService scoreService;
	
	@Autowired
	private DashboardService dashboardService;
	
	@Autowired
	private BanCountCache banCountCache;

	@RequestMapping(value = "/pie/chart/all", method = RequestMethod.GET)
	public ResponseEntity<HashMap<String, List<PieChartData>>> getAllChartData() {
		HashMap<String, List<PieChartData>> allChartData = countService.getAllChartData();
		return new ResponseEntity<HashMap<String,List<PieChartData>>>(allChartData, HttpStatus.OK);
	}

	/*
	 * header 비율
	 */
	@RequestMapping(value = "/header/ratio", method = RequestMethod.GET)
	public ResponseEntity<ResponseHeaderList> getHeaderRatio(HttpServletRequest request) {
		List<HeaderRatio> headerRatios = new ArrayList<>();
		Enumeration<String> headerNames = request.getHeaderNames();
		if (headerNames != null) {
			while (headerNames.hasMoreElements()) {
				final String headerName = headerNames.nextElement();
				final String headerValue = request.getHeader(headerName);
				final String ratio = countService.getRatio(headerName, headerValue);
				HeaderRatio headerRatio = new HeaderRatio();
				headerRatio.setKey(headerName);
				headerRatio.setValue(headerValue);
				headerRatio.setRatio(ratio);
				headerRatios.add(headerRatio);
			}
		}
		ResponseHeaderList responseHeaderList = new ResponseHeaderList();
		responseHeaderList.setHeaderRatios(headerRatios);
		return new ResponseEntity<ResponseHeaderList>(responseHeaderList, HttpStatus.OK);
	}
	
	// 차단 이유 (이유, 횟수)
	@RequestMapping(value = "/reason/ban", method = RequestMethod.GET)
	public ResponseEntity<ResponseBanReason> banReasons() {
		ResponseBanReason responseBanReason = new ResponseBanReason();
		responseBanReason.setReasons(dashboardService.getBanReason());
		return new ResponseEntity<ResponseBanReason>(responseBanReason, HttpStatus.OK);
	}
	
	// 차단된 유저 (시간, 차단유저수)
	@RequestMapping(value = "/count/ban", method = RequestMethod.GET)
	public ResponseEntity<ResponseBanCount> banCount() {
		ResponseBanCount responseBanCount = new ResponseBanCount();
		List<ChartData> all = banCountCache.getAll();
		int count = all.size();
		if (count < 120) {
			long oldTime = System.currentTimeMillis();
			for (ChartData chartData : all) {
				Long date = chartData.getRes().get(0);
				if (date < oldTime) {
					oldTime = date;
				}
			}
			
			while (count < 120) {
				oldTime = oldTime - TimeUnit.MINUTES.toMillis(1);
				banCountCache.putPre(oldTime, 0);
				count++;
			}
			all = banCountCache.getAll();
		}
		
		responseBanCount.setChartDatas(all);
		return new ResponseEntity<ResponseBanCount>(responseBanCount, HttpStatus.OK);
	}
	
	// Score Table
	// user, 스코어 이유, 몇점, 언제
	@RequestMapping(value = "/score", method = RequestMethod.GET)
	public ResponseEntity<Page<ScoreInfo>> banReasons(Pageable pageable) {
		Page<ScoreInfo> allScore = scoreService.getAllScore(pageable);
		return new ResponseEntity<Page<ScoreInfo>>(allScore, HttpStatus.OK);
	}
}