package com.skplanet.secu.login.geo;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class GeoHelper {

	@Autowired
	private IPReader ipReader;

	public String getLocation(HttpServletRequest request) {
		String userIp = request.getRemoteAddr();
		return getLocation(userIp);
	}
	
	public String getLocation(String ip) {
		try {
			GeoLocation geoLocation = ipReader.lookup(ip);
			if (geoLocation != null) {				
				System.out.println("getCountry >> " + geoLocation.getCountry());
				System.out.println("getFullCountry >> " + geoLocation.getFullCountry());
				System.out.println("startIP >> " + geoLocation.getCoord().getLatitude()); // 26.061390
				System.out.println("endIP >> " + geoLocation.getCoord().getLongitude()); // 119.306110
				return geoLocation.getCountry();
			} else {
				return null;
			}
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}
//	
//	public SubMain() {
//		try {
//			ipReader = IPReader.build(getClass().getResource("/GeoIPCountryWhois.csv").getPath());
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//	}
//	
//	public static void main(String[] args) {
//		SubMain subMain = new SubMain();
//		String location = subMain.getLocation("216.58.220.196");
//		System.out.println(location);
//	}
}