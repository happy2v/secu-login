package com.skplanet.secu.login.controller.model;

import javax.validation.constraints.NotNull;

public class AuthKey {

	@NotNull
	private String authKey;

	public String getAuthKey() {
		return authKey;
	}

	public void setAuthKey(String authKey) {
		this.authKey = authKey;
	}
}
