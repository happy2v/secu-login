package com.skplanet.secu.login.controller.backup;

import java.io.IOException;
import java.util.UUID;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;


/**
 * Created by happy2v on 16. 1. 25.
 */
@Controller
public class JSController {

	@RequestMapping(value = "/uuid", method = RequestMethod.GET, produces = { MediaType.TEXT_PLAIN_VALUE })
	public ResponseEntity<String> getUUID() {
		final String uuid = UUID.randomUUID().toString();
		return new ResponseEntity<String>(uuid, HttpStatus.OK);
	}

//	@RequestMapping(value = "/iptest", method = RequestMethod.GET)
//	public ResponseEntity<String> iptest() throws IOException {
//		
//		SubMain sMain = new SubMain();
//		sMain.test("124.66.184.4");
//		return new ResponseEntity<String>(HttpStatus.OK);
//	}
}