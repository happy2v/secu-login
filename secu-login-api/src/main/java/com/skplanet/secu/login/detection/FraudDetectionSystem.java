package com.skplanet.secu.login.detection;

import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import com.skplanet.secu.login.db.entity.SessionInfo;
import com.skplanet.secu.login.db.entity.UserInfo;
import com.skplanet.secu.login.db.repo.SessionRepo;
import com.skplanet.secu.login.db.repo.UserInfoRepo;
import com.skplanet.secu.login.db.service.AccessService;
import com.skplanet.secu.login.db.service.ScoreService;

@Component
public class FraudDetectionSystem {

	@Autowired
	UserInfoRepo userRepo;

	@Autowired
	SessionRepo sessionRepo;

	@Autowired
	AccessService accServ;
	
	@Autowired
	private ScoreService scoreService;

	public ResponseEntity<Long> compareLocation(String id, String sessionKey) {
		checkFdsScore(sessionKey);
		UserInfo userIF = userRepo.findByUserId(id);
		SessionInfo sessionIF = sessionRepo.findBySessionKey(sessionKey);
		if (userIF == null) {
			System.out.println("no exist user");
		} else {
			String nation = userIF.getNation();
			if (nation != null) {				
				if (!nation.equals(sessionIF.getAccessCountry())) {
					// 吏��뿭 蹂�寃�
					System.out.println("FDS :: access country is diffrent with first access country");
					scoreService.addScore(id, "access country is diffrent with first access country", 1);
					if (sessionIF.getFDSscore() == 0) {
						sessionIF.setFDSscore(1);
					} else {
						sessionIF.setFDSscore(sessionIF.getFDSscore() + 1);
					}
					return new ResponseEntity<Long>(HttpStatus.CONFLICT);
				}
			}
			System.out.println("Noerror");
		}

		return new ResponseEntity<Long>(HttpStatus.OK);
	}

	public ResponseEntity<Long> anomalyAccess(String header, String sessionKey) {
		checkFdsScore(sessionKey);
		int count = -1;// to ignore first access(sign up)
		SessionInfo sessionIF = sessionRepo.findBySessionKey(sessionKey);
		long currentTimeMillis = System.currentTimeMillis();

		long millis = TimeUnit.MINUTES.toMillis(30);
		Date date = new Date(currentTimeMillis - millis);
		System.out.println("current time - 30 min:: " + date);
		List<SessionInfo> findByCreatedDateAfter = sessionRepo.findByCreatedDateAfter(date);

		List<String> currentHeader = accServ.parsingHeader(sessionKey);
		int size = currentHeader.size();
		int cnt = 0;
		int cntForFifthfail = 0;
		for (SessionInfo sessionInfo : findByCreatedDateAfter) {
			System.out.println("########################## get header::" + sessionInfo.getHeader());
			List<String> pastHeader = accServ.parsingHeader(sessionInfo.getSessionKey());
			for (int i = 0; i < size; i++) {
				if (pastHeader.get(i).toString().equals(currentHeader.get(i).toString())) {
					cnt++;
				}
			}
			if (cnt == size) {
				count++;

				if (sessionInfo.getStateOfTrial().equals("signin::fail")) {
					cntForFifthfail++;
					System.out.println("fail count>>" + cntForFifthfail);
				}
				if (cntForFifthfail == 5) {
					// 5踰� �씠�긽 �떎�뙣 
					scoreService.addScore(sessionIF.getUserId(), "number of login fail is over 5", 1);
					System.out.println("number of login fail is over 5!!");
					if (sessionIF.getFDSscore() == 0) {
						sessionIF.setFDSscore(1);
					} else {
						sessionIF.setFDSscore(sessionIF.getFDSscore() + 1);
					}
					//// return!!!!
				}
				System.out.println("****login trial count within 30 min::::" + count);
				if (count >= 6) {
					
					// 30遺� �씠�궡 10踰� �씠�긽 �떆�룄
					scoreService.addScore(sessionIF.getUserId(), "login trial count within 30 min", 3);
					System.out.println("Anomaly Access!!");
					if (sessionIF.getFDSscore() == 0) {
						sessionIF.setFDSscore(3);
					} else {
						sessionIF.setFDSscore(sessionIF.getFDSscore() + 3);
					}
					return new ResponseEntity<Long>(HttpStatus.FORBIDDEN);
				}
			}
		}
		return new ResponseEntity<Long>(HttpStatus.CONTINUE);
	}

	public void fiveTrials(String sessionKey) {
		SessionInfo sessionIF = sessionRepo.findBySessionKey(sessionKey);
		scoreService.addScore(sessionIF.getUserId(), "over five times trials", 1);
		System.out.println("over five times trials!!");
		if (sessionIF.getFDSscore() == 0) {
			sessionIF.setFDSscore(1);
		} else {
			sessionIF.setFDSscore(sessionIF.getFDSscore() + 1);
		}
		checkFdsScore(sessionKey);
	}

	public ResponseEntity<Long> checkFdsScore(String sessionKey) {
		SessionInfo sessionIF = sessionRepo.findBySessionKey(sessionKey);
		if (sessionIF.getFDSscore() >= 5) {
			System.out.println("High FDS score!!");
			return new ResponseEntity<Long>(HttpStatus.FORBIDDEN);
		}
		return new ResponseEntity<Long>(HttpStatus.OK);
	}
}
