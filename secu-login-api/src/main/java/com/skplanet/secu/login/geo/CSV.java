package com.skplanet.secu.login.geo;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/*public class CSV {
    private static final java.lang.String CSV_PATTERN = "\"([0-9]+)\",\"([0-9]+)\",\"([^\"]+)\",\"([^\"]+)\",\"([^\"]+)\",\"([^\"]+)\",\"([0-9.-]+)\",\"([0-9.-]+)\"";

    private final Long ipStart;
    private final Long ipEnd;
    private final String country;
    private final String stateProv;
    private final String city;
    private final Double latitude;
    private final Double longitude;

    private CSV(Long ipStart, Long ipEnd,
                String country, String stateProv, String city,
                Double latitude, Double longitude) {
        this.ipStart = ipStart;
        this.ipEnd = ipEnd;
        this.country = country;
        this.stateProv = stateProv;
        this.city = city;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public static CSV parse(String csv) {
        Pattern r = Pattern.compile(CSV_PATTERN);
        Matcher m = r.matcher(csv);

        if (m.find()) {
            return new CSV(
                    Long.parseLong(m.group(1)),
                    Long.parseLong(m.group(2)),
                    m.group(4),
                    m.group(5),
                    m.group(6),
                    Double.parseDouble(m.group(7)),
                    Double.parseDouble(m.group(8))
            );
        }

        throw new IllegalArgumentException(String.format("CSV doesn't match the pattern. (%s)", csv));
    }

    public Long getIpStart() {
        return ipStart;
    }

    public Long getIpEnd() {
        return ipEnd;
    }

    public String getCountry() {
        return country;
    }

    public String getStateProv() {
        return stateProv;
    }

    public String getCity() {
        return city;
    }

    public Double getLatitude() {
        return latitude;
    }

    public Double getLongitude() {
        return longitude;
    }
}
*/

public class CSV {
	private static final java.lang.String CSV_PATTERN = "\"([^\"]+)\",\"([^\"]+)\",\"([0-9.-]+)\",\"([0-9.-]+)\",\"([^\"]+)\",\"([^\"]+)\"";

	private final String ipStart;
	private final String ipEnd;
	private final Long latitude;
	private final Long longitude;
	private final String country;
	private final String fullContry;
	// private final String stateProv;
	// private final String city;

	private CSV(String ipStart, String ipEnd, Long latitude, Long longitude, String country, String fullContry) {
		this.ipStart = ipStart;
		this.ipEnd = ipEnd;
		this.country = country;
		this.fullContry = fullContry;
		this.latitude = latitude;
		this.longitude = longitude;
	}

	public static CSV parse(String csv) {
		Pattern r = Pattern.compile(CSV_PATTERN);
		Matcher m = r.matcher(csv);

		if (m.find()) {
			return new CSV(m.group(1), m.group(2), Long.parseLong(m.group(3)), Long.parseLong(m.group(4)), m.group(5),
					m.group(6));
		}

		throw new IllegalArgumentException(String.format("CSV doesn't match the pattern. (%s)", csv));
	}

	public String getIpStart() {
		return ipStart;
	}

	public String getIpEnd() {
		return ipEnd;
	}

	public String getCountry() {
		return country;
	}

	public String getFullCountry() {
		return fullContry;
	}

	public Long getLatitude() {
		return latitude;
	}

	public Long getLongitude() {
		return longitude;
	}
}
