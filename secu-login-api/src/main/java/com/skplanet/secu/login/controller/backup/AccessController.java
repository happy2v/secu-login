package com.skplanet.secu.login.controller.backup;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.skplanet.secu.login.db.entity.SessionInfo;
import com.skplanet.secu.login.db.repo.SessionRepo;
import com.skplanet.secu.login.db.service.AccessService;

@Controller
public class AccessController {

	@Autowired
	private SessionRepo sessionRepo;

	@Autowired
	private AccessService accessSev;

	/*
	 * @RequestMapping(value = "/test", method = RequestMethod.GET) public
	 * ResponseEntity<String> testGet(@RequestParam(value = "data", required =
	 * true) String data) { String response = "hello, " + data; return new
	 * ResponseEntity<String>(response, HttpStatus.OK); }
	 * 
	 * @RequestMapping(value = "/test", method = RequestMethod.POST) public
	 * ResponseEntity<String> testPost(@RequestBody TestReq testReq) { String
	 * data = testReq.getData(); String response = "hello, " + data; return new
	 * ResponseEntity<String>(response, HttpStatus.OK); }
	 */

	@RequestMapping(value = "/access", method = RequestMethod.GET)
	public String testGet2(@RequestParam("ID") String value, @RequestParam("state") String state,
			HttpServletRequest request) throws IOException {

		accessSev.getAccessInfo(value, state, request, 0);

		if (state.equals("fail"))
			return "redirect:/fail";
		else
			return "redirect:/success";
	}

	@RequestMapping("/parsing")
	public ResponseEntity<String> parsingH(@RequestParam("SSkey") String value) {
		SessionInfo ssinfo = sessionRepo.findBySessionKey(value);
		String header = ssinfo.getHeader();
		StringTokenizer st = new StringTokenizer(header, "#");
		// List<String> headerL = new ArrayList<String>();
		List<String> headerL = new ArrayList<String>();
		while (st.hasMoreTokens()) {
			headerL.add(st.nextToken());
		}
		final StringBuilder sb = new StringBuilder();
		for (int i = 0; i < headerL.size(); i++) {
			// System.out.println(headerL.get(i).toString()+"\n");
			sb.append(headerL.get(i).toString() + "<br>");
		}
		return new ResponseEntity<String>(sb.toString(), HttpStatus.OK);
	}

}
