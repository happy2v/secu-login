package com.skplanet.secu.login.db.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.skplanet.secu.login.db.entity.SessionInfo;
import com.skplanet.secu.login.db.entity.UserInfo;
import com.skplanet.secu.login.db.model.SessionState;
import com.skplanet.secu.login.db.repo.SessionRepo;
import com.skplanet.secu.login.db.repo.UserInfoRepo;

import java.util.List;

/**
 * Created by happy2v on 2016. 2. 14..
 */
@Component
@Transactional(readOnly = false)
public class AuthService {

    @Autowired
    private UserInfoRepo userInfoRepo;

    @Autowired
    private SessionRepo sessionRepo;
    
    public boolean existUser(String id) {
        final UserInfo user = userInfoRepo.findByUserId(id);
        if (user == null) {
            return false;
        } else {
            return true;
        }
    }

    public void signup(String id, String pw, String email, String nation) {
        UserInfo user = userInfoRepo.findByUserId(id);
 
        user = new UserInfo();
        user.setUserId(id);
        user.setPassword(pw);
        user.setEmail(email);
        user.setNation(nation);
        userInfoRepo.save(user);
    }

    public Boolean signin(String id, String pw) {
        final UserInfo user = userInfoRepo.findByUserId(id);
        if (user == null) {
            return false;
        }
        if (user.getPassword().equals(pw)) {
            return true;
        } 
        else {
//            final String sessionKey = UUID.randomUUID().toString();
//            SessionInfo sessionInfo = new SessionInfo();
//            sessionInfo.setSessionKey(sessionKey);
//            sessionInfo.setUserId(id);
//            sessionInfo.setState(SessionState.ACTIVE.name());
//            sessionRepo.save(sessionInfo);
//            return sessionKey;
        	return false;
        }
    }

    public void signout(String sessionKey) {
        final SessionInfo sessionInfo = sessionRepo.findBySessionKey(sessionKey);
        if (sessionInfo != null) {
            sessionInfo.setState(SessionState.EXPIRED.name());
        }
    }

    public String getUserId(String sessionKey) {
        final SessionInfo sessionInfo = sessionRepo.findBySessionKey(sessionKey);
        if (sessionInfo.getState().equals(SessionState.ACTIVE.name())) {
            return sessionInfo.getUserId();
        } else {
            return null;
        }
    }
    
    public String getSessionId(String userId) {
    	final List<SessionInfo> sessionInfo = sessionRepo.findByUserId(userId);
    	if (sessionInfo != null) {
            SessionInfo sessionInfo1 = sessionInfo.get(sessionInfo.size() - 1);
            return sessionInfo1.getSessionKey();
    	}
    	return null;
    }
}
