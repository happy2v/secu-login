package com.skplanet.secu.login;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@ComponentScan(basePackageClasses = { SwaggerConfig.class })
@EnableScheduling
public class SecuLoginApplication {

	public static void main(String[] args) {
		SpringApplication.run(SecuLoginApplication.class);
	}
}