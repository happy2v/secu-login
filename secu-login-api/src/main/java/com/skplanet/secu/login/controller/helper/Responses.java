package com.skplanet.secu.login.controller.helper;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.skplanet.secu.login.controller.model.ResponseData;

public class Responses {

	public static ResponseEntity<ResponseData> responseString(String data, HttpStatus status) {
		final ResponseData responseData = new ResponseData();
		responseData.setData(data);
		return new ResponseEntity<ResponseData>(responseData, status);
	}
	
	public static ResponseEntity<ResponseData> response(HttpStatus status) {
		return new ResponseEntity<ResponseData>(status);
	}
}
