package com.skplanet.secu.login;

import java.io.IOException;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;

import com.skplanet.secu.login.geo.IPReader;

import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2()
public class SwaggerConfig {
    public PropertySourcesPlaceholderConfigurer config () {
        PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer = new PropertySourcesPlaceholderConfigurer();
        propertySourcesPlaceholderConfigurer.setIgnoreUnresolvablePlaceholders(true);
        return propertySourcesPlaceholderConfigurer;
    }
    
    @Bean
    public IPReader ipReaderBean() {
    	try {
			return IPReader.build(getClass().getResource("/GeoIPCountryWhois.csv").getPath());
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
    }
}