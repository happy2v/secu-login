package com.skplanet.secu.login.db.service;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;

import javax.servlet.http.HttpServletRequest;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.skplanet.secu.login.cache.CountCache;
import com.skplanet.secu.login.controller.model.PieChartData;
import com.skplanet.secu.login.db.entity.CountInfo;
import com.skplanet.secu.login.db.entity.TotalInfo;
import com.skplanet.secu.login.db.repo.CountRepo;
import com.skplanet.secu.login.db.repo.TotalRepo;

/**
 * Created by happy2v on 2016. 2. 14..
 */
@Component
@Transactional(readOnly = false)
public class CountService {
	private final Logger logger = LogManager.getLogger(CountService.class);

	@Autowired
	private CountRepo countRepo;
	@Autowired
	private TotalRepo totalRepo;
	@Autowired
	private CountCache countCache;

	public void requestCounting(HttpServletRequest request) {
		countingHeader(request);
	}

	public String getRatio(String key, String value) {
		Long totalCnt = countCache.getTotalCount(key);
		Long valueCnt = countCache.getValueCount(key, value);
		
		logger.info("Total Count [{}], value Count [{}]", totalCnt, valueCnt);

		double probability = (double) valueCnt / totalCnt * 50;
		String ratioStr = String.valueOf(probability);
		int indexOf = ratioStr.indexOf(".");
		if (indexOf + 3 < ratioStr.length()) {
			ratioStr = ratioStr.substring(0, ratioStr.indexOf(".") + 3);
		}
		return ratioStr;
	}
		
	public List<PieChartData> getChartData(String key) {
		List<PieChartData> pieChartDatas = new ArrayList<>();
		ConcurrentHashMap<String, ConcurrentHashMap<String, AtomicLong>> keyValueCount = countCache.getKeyValueCount();
		ConcurrentHashMap<String, AtomicLong> concurrentHashMap = keyValueCount.get(key);
		Enumeration<String> values = concurrentHashMap.keys();
		while (values.hasMoreElements()) {
			String value = (String) values.nextElement();
			AtomicLong atomicLong = concurrentHashMap.get(value);
			PieChartData pieChartData = new PieChartData();
			pieChartData.setLabel(value);
			pieChartData.setData(atomicLong.get());
			pieChartDatas.add(pieChartData);
		}
		return pieChartDatas;
	}
	
	public HashMap<String, List<PieChartData>> getAllChartData() {
		HashMap<String, List<PieChartData>> allChartData = new HashMap<>();
		ConcurrentHashMap<String, ConcurrentHashMap<String, AtomicLong>> keyValueCount = countCache.getKeyValueCount();
		Enumeration<String> keys = keyValueCount.keys();
		while (keys.hasMoreElements()) {
			String key = (String) keys.nextElement();
			List<PieChartData> chartData = getChartData(key);
			allChartData.put(key, chartData);
		}
		return allChartData;
	}

	private void countingHeader(HttpServletRequest request) {
		Enumeration<String> headerNames = request.getHeaderNames();
		if (headerNames != null) {
			long startAll = System.currentTimeMillis();
			while (headerNames.hasMoreElements()) {
				String headerName = headerNames.nextElement();
				String headerValue = request.getHeader(headerName);

				/*
				 * boolean containsValue = countCache.containsValue(headerName,
				 * headerValue); if (!containsValue) { Long countValue =
				 * getCountValue(headerName, headerValue);
				 * countCache.setValueCount(headerName, headerValue,
				 * countValue); }
				 */
				boolean containsKey = countCache.containsKey(headerName);
				if (!containsKey) {
					Long countValue = getCountKey(headerName);
					countCache.setKeyCount(headerName, countValue);
				}
				countCache.count(headerName, headerValue);
			}
			//
			/*
			 * String userIp = request.getRemoteAddr(); Long countV =
			 * getCountValue("user ip", userIp); countCache.setValueCount(
			 * "user ip", userIp, countV); int clientPort =
			 * request.getRemotePort(); countV = getCountValue("port number",
			 * String.valueOf(clientPort)); countCache.setValueCount(
			 * "port number", String.valueOf(clientPort), countV);
			 */
			Long countValue = getCountKey("user ip");
			countCache.setKeyCount("user ip", countValue);
			countValue = getCountKey("port number");
			countCache.setKeyCount("port number", countValue);

			boolean containsKey = countCache.containsKey("user ip");
			if (!containsKey) {
				Long cv = getCountKey("user ip");
				countCache.setKeyCount("user ip", cv);
			}
			String userIp = request.getRemoteAddr();
			countCache.count("user ip", userIp);

			containsKey = countCache.containsKey("port number");
			if (!containsKey) {
				Long cv = getCountKey("port number");
				countCache.setKeyCount("port number", cv);
			}
			int clientPort = request.getRemotePort();
			countCache.count("port number", String.valueOf(clientPort));
			//
			logger.info("During all time [{}ms]", (System.currentTimeMillis() - startAll));
		}
	}

	private Long getCountValue(String key, String value) {
		CountInfo countInfo = countRepo.findByAttributeKey(key + value);
		if (countInfo == null) {
			countInfo = new CountInfo();
			countInfo.setAttributeKey(key + value);
			countInfo.setAttribute(value);
			countInfo.setCount(0L);
			countInfo.setName(key);
			countRepo.save(countInfo);
		}
		return countInfo.getCount();
	}

	private Long getCountKey(String key) {
		TotalInfo totalInfo = totalRepo.findByInfoName(key);
		if (totalInfo == null) {
			totalInfo = new TotalInfo();
			totalInfo.setInfoName(key);
			totalInfo.setTotalCount(0L);
			totalRepo.save(totalInfo);
		}
		return totalInfo.getTotalCount();
	}

	public void printCount() {
		List<CountInfo> countInfos = countRepo.findAll();
		for (CountInfo countInfo : countInfos) {
			String name = countInfo.getName();
			TotalInfo totalInfo = totalRepo.findByInfoName(name);
			if (totalInfo != null) {
				logger.info("Count [{}/{}][{}:{}]", countInfo.getCount(), totalInfo.getTotalCount(), name,
						countInfo.getAttribute());
			} else {
				logger.info("Count [{}/{}][{}:{}]", countInfo.getCount(), 0, name, countInfo.getAttribute());
			}
		}
	}

	// Get data from DB to cache
	public void initCount() {
		List<CountInfo> countInfos = countRepo.findAll();
		for (CountInfo countInfo : countInfos) {
			countCache.setValueCount(countInfo.getName(), countInfo.getAttribute(), countInfo.getCount());
		}
		List<TotalInfo> totalInfos = totalRepo.findAll();
		for (TotalInfo totalInfo : totalInfos) {
			countCache.setKeyCount(totalInfo.getInfoName(), totalInfo.getTotalCount());
		}
	}

	// Set date from cache to DB
	public void syncCount() {
		ConcurrentHashMap<String, AtomicLong> valueCount = countCache.getValueCount();
		ConcurrentHashMap<String, AtomicLong> keyCount = countCache.getKeyCount();
		HashMap<String, String> valueData = countCache.getValueData();
		HashMap<String, String> valueName = countCache.getValueName();

		Enumeration<String> values = valueCount.keys();
		// while(values.hasMoreElements()) {
		// final String value = values.nextElement();
		// final AtomicLong atomicLong = valueCount.get(value);
		// CountInfo countIF = countRepo.findByAttributeKey(value);
		// if(countIF == null){
		// countIF = new CountInfo();
		// countIF.setAttribute(valueData.get(value));
		// countIF.setAttributeKey(value);
		// countIF.setCount(atomicLong.get());
		// countIF.setName(valueName.get(value));
		// }
		// else{
		// countIF.setCount(countIF.getCount()+1);
		// }
		//
		// countRepo.save(countIF);
		// }

		Enumeration<String> keys = keyCount.keys();
		while (keys.hasMoreElements()) {
			final String key = keys.nextElement();
			AtomicLong atomicLong = keyCount.get(key);
			if (atomicLong == null) {
				countCache.setKeyCount(key, 1L);
				atomicLong = keyCount.get(key);
			}
			TotalInfo totalInfo = totalRepo.findByInfoName(key);
			if (totalInfo == null) {
				totalInfo = new TotalInfo();
				totalInfo.setInfoName(key);
				totalInfo.setTotalCount(atomicLong.get());
			} else {
				totalInfo.setTotalCount(atomicLong.get());
			}
			totalRepo.save(totalInfo);
		}
		// final String key = keys.nextElement();
		AtomicLong atomicLong = keyCount.get("user ip");
		if (atomicLong == null) {
			countCache.setKeyCount("user ip", 1L);
			atomicLong = keyCount.get("user ip");
		}
		TotalInfo totalInfo = totalRepo.findByInfoName("user ip");
		if (totalInfo == null) {
			totalInfo = new TotalInfo();
			totalInfo.setInfoName("user ip");
			totalInfo.setTotalCount(atomicLong.get());
			totalRepo.save(totalInfo);
		} else {
			totalInfo.setTotalCount(atomicLong.get());
		}

		AtomicLong atomicL = keyCount.get("port number");
		if (atomicL == null) {
			countCache.setKeyCount("port number", 1L);
			atomicL = keyCount.get("port number");
		}
		TotalInfo totalIF = totalRepo.findByInfoName("port number");
		if (totalIF == null) {
			totalIF = new TotalInfo();
			totalIF.setInfoName("port number");
			totalIF.setTotalCount(atomicL.get());
			totalRepo.save(totalIF);
		} else {
			totalIF.setTotalCount(atomicL.get());
		}
	}
}
